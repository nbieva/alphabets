---
title: Le texte avec P5*js
lang: fr-FR
---

# Le texte avec P5*js

https://www.pagedjs.org/documentation/

Vous trouverez ci-dessous quelques exemples de scripts qui travaillent avec le texte.

## Capter le clavier

```javascript
function setup() {
  createCanvas(740, 370);
  background(50);
  noStroke();
  //On centre le texte sur les coordonnées X et Y renseignées
  textAlign(CENTER, CENTER);
  textSize(100);
  fill(random(255), random(255), random(255));
  text("Tape une lettre", width/2, height/2);
}

function keyTyped() {
  background(50);
  fill(random(255), random(255), random(255));
  text(key, width/2, height/2);
  return false; // prevent default
}
```

<iframe class="p5embed" width="740" height="370" src="https://editor.p5js.org/nicobiev/embed/zbDlMpvU"></iframe>

### Fonctions utilisées:

+ [keyTyped()](https://p5js.org/reference/#/p5/keyTyped): La fonction keyTyped est exécutée chaque fois qu'une touche du clavier est pressée. Elle ne prend pas en compte Ctrl, Shift, etc... Pour cela, utilisez [keyPressed()](https://p5js.org/reference/#/p5/keyPressed)
+ [textSize()](https://p5js.org/reference/#/p5/textSize)
+ [textAlign()](https://p5js.org/reference/#/p5/textAlign)
+ [key](https://p5js.org/reference/#/p5/key): Notez que **key** est une variable et ne prend donc pas de paramètres. Elle se comporte un peu comme le ferait **mouseX** et se *contente* de récupérer une valeur.
+ [random()](https://p5js.org/reference/#/p5/random): la fonction random() se comporte comme celle que nous utilisions dans Processing.

## keyCode

The variable keyCode is used to detect special keys such as BACKSPACE, DELETE, ENTER, RETURN, TAB, ESCAPE, SHIFT, CONTROL, OPTION, ALT, UP_ARROW, DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW. You can also check for custom keys by looking up the keyCode of any key on a site like this: keycode.info.

## Pattern

Dans le code ci-dessous, nous utilisons notre propre police de caractères, ainsi que des boucles imbriquées pour obtenir un rendu en temps réel.

<iframe class="p5embed" width="740" height="370" src="https://editor.p5js.org/nicobiev/embed/p0BrzZ9R"></iframe>

Il y a beaucoup de variables sur lesquelles vous pouvez travailler pour la suite:

+ Le texte lui-même (caractère (**char**), chaîne de caractères (**String**), ...)
+ La casse
+ La taille du texte
+ Contour et remplissage + différents types de contours
+ La couleur (Rouge, vert, bleu, couche alpha, teinte, saturation, luminosité...)
+ Décalage
+ Modes de fusion (**blendMode**)
+ Background (peut-être aussi une image..)
+ Superpositions
+ transformations (rotation, mise à l'échelle)
+ Créer des versions animées de vos motifs
+ En faire un film

N'oubliez pas d'exportez vos images.
Mettez en ligne sur le web. Postez sur Instagram #alphabits
Créer des groupes de lettres

```javascript
let myFont;
let posx, posy, nombre, decalage;

function preload() {
  myFont = loadFont('babnouschka.ttf');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(50);
  noStroke();
  textFont(myFont);
  textAlign(CENTER, CENTER);
  textSize(80);
  nombre = 20;
  decalage = width/nombre;
  posx = decalage/2;
  posy = 0;
  fill(255);
  for(let i=0;i<height+decalage;i+=decalage) {
    for(let j=decalage/2;j<width;j+=decalage) {
      text("A", j, i);
    }
  }
}

function keyTyped() {
  background(50);
  fill(255);
  for(let i=0;i<height+decalage;i+=decalage) {
    for(let j=decalage/2;j<width;j+=decalage) {
      text(key, j, i);
    }
  }
}
```

## Créer une interface

Nous allons ensuite créer une interface minimale pour manipuler nos variables en temps réel. Concrètement, nous utilisons déjà une interface (notre **clavier**) pour manipuler une de nos variables (**key**).

Nous pourrions d'ailleurs créer une interface générale nous permettant même de choisir notre police dans une liste (menu déroulant).

Vous pouvez aussi faire réagir telle ou telle variable au niveau sonore capté par le micro.