---
title: Le texte avec P5*js
lang: fr-FR
---

# Le texte avec P5*js

Vous trouverez ci-dessous quelques exemples de scripts qui travaillent avec le texte.

## Capter le clavier

```javascript
function setup() {
    // On récupère la largeur et la hauteur de la page
    createCanvas(windowWidth, windowHeight);
    background(50);
    noStroke();
    //On centre le texte sur les coordonnées X et Y renseignées
    textAlign(CENTER, CENTER);
    //Je vous laisse deviner...
    textSize(600);
}

function draw() {
  fill(random(255), random(255), random(255), random(255));
  ellipse(random(width), random(height), 5, 5);
}

// La fonction keyTyped est exécutée chaque fois qu'une touche est pressée
// Elle ne prend pas en compte Ctrl, Shift, etc...
function keyTyped() {
  background(50);
  translate(width/2, height/2);
  fill(255);
  fill(random(255), random(255), random(255), random(255));
  text(key, 0, 0);
  return false; // prevent default
}
```

### Fonctions utilisées:

+ [keyTyped()](https://p5js.org/reference/#/p5/keyTyped): La fonction keyTyped est exécutée chaque fois qu'une touche du clavier est pressée. Elle ne prend pas en compte Ctrl, Shift, etc... Pour cela, utilisez [keyPressed()](https://p5js.org/reference/#/p5/keyPressed)
+ [textSize()](https://p5js.org/reference/#/p5/textSize)
+ [textAlign()](https://p5js.org/reference/#/p5/textAlign)
+ [key](https://p5js.org/reference/#/p5/key): Notez que **key** est une variable et ne prend donc pas de paramètres. Elle se comporte un peu comme le ferait **mouseX** et se *contente* de récupérer une valeur.
+ [random()](https://p5js.org/reference/#/p5/random): la fonction random() se comporte comme celle que nous utilisions dans Processing.

## keyCode

The variable keyCode is used to detect special keys such as BACKSPACE, DELETE, ENTER, RETURN, TAB, ESCAPE, SHIFT, CONTROL, OPTION, ALT, UP_ARROW, DOWN_ARROW, LEFT_ARROW, RIGHT_ARROW. You can also check for custom keys by looking up the keyCode of any key on a site like this: keycode.info.