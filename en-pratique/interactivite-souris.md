---
title: Interactivité souris
lang: fr-FR
---

# Interactivité souris

## Avec la souris

<a data-fancybox title="Pointer, pointer" href="/assets/mouse.png">![Pointer, pointer](/assets/mouse.png)</a>
Jonathan Puckey, [*Pointer, pointer*](https://pointerpointer.com/)

Pointer Pointer is an experiment in interaction, a celebration of the disappearing mouse pointer and a rumination on Big Data.
(+ https://www.youtube.com/watch?v=Z2ZXW2HBLPM )

## Un outil de dessin

```processing
void setup() {
    // dimension du canvas
    size(800,600);
    // fond blanc
    background(255);
}

void draw(){
    // on dessine une ellipse dont les positions x et y correspondent à la position de la souris.
    ellipse(mouseX, mouseY, 20, 20);
}
```

## Changer de couleur au clic

```processing

void setup() {
    size(600, 600);
    background(255);
}

void draw(){
}

// fonction détectant si l'on clique sur la souris
void mouseClicked() {
    background(random(255), random(255), random(255)); // on change la couleur du fond aléatoirement
}

```