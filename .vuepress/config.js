module.exports = {
    title: 'AlphaBits',
    description: 'Support en ligne pour le CASO Code B3',
    head: [
      ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.slim.min.js' }],
      ['script', { src: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.js' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.2/jquery.fancybox.min.css' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,700i,900,900i&display=swap' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/icon?family=Material+Icons' }],
      ['link', { rel: 'stylesheet', type: 'text/css', href: '/assets/styles.css' }]
  ],
    markdown: {
      lineNumbers: true
    },
    themeConfig: {
        nav: [
          { text: 'Home', link: '/' },
          { text: 'Processing', link: 'https://processing.org' },
          { text: 'P5js', link: 'https://p5js.org' }
        ],
        sidebarDepth: 0,
        sidebar: [
            {
              title: 'Alphabits',
              collapsable: false,
              children: [
                '/',
                'generalites/filiation',
                'generalites/inspirations',
                'generalites/processing-et-p5js',
                'generalites/outils'
              ]
            },
            {
              title: 'Petit rappel...',
              collapsable: true,
              children: [
                'modules-b3/module1',
                'modules-b3/module2'
              ]
            },
            {
              title: 'En théorie',
              collapsable: false,
              children: [
                'processing/structure',
                'processing/coordonnees',
                'processing/formes-simples',
                'processing/couleur',
                'processing/transformations',
                'processing/variables',
                'processing/conditions',
                'processing/boucles',
                'processing/fonctions',
                'processing/aleatoire',
              ]
            },
            
            {
              title: 'En pratique',
              collapsable: false,
              children: [
                'en-pratique/animation',
                'en-pratique/images',
                'en-pratique/charger-svg',
                'en-pratique/interactivite-souris',
                'en-pratique/interactivite-clavier',
                'en-pratique/texte',
                'en-pratique/film',
                'en-pratique/exporter',
                'modules-b3/tableaux',
                'modules-b3/images'
              ]
            }
          ]
    }
}