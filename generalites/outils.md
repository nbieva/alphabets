---
title: Outils et bonnes adresses
lang: fr-FR
---

# Outils et bonnes adresses

## Langages

+ Le site de [Processing](https://processing.org/). Processing est construit sur le langage Java (à ne pas confondre avec JavaScript)
+ Le site de [P5js](https://p5js.org/). P5js est écrit en JavaScript, idéal pour le web donc.
+ Le site de **[Scratch](https://scratch.mit.edu/)** (programmation par blocs)
+ Le site de **[Micro:bit](https://microbit.org/fr/)** créé par la BBC. (Blocs ou javaScript)
+ Le site du nano ordinateur [Raspberry Pi](https://www.raspberrypi.org/). On peut utiliser Processing sur un Raspberry Pi, ou Javascript, via Nod-red, mais le langage peut-être le plus utilisé (dans les tutoriels et autres..) est ici [Python](https://www.python.org/).
+ [Node-red](https://nodered.org/) (Par blocs et noeuds. Javascript)

## Les éditeurs de code

L'éditeur de code est l'outil indispensable pour travailler vos langages préférés. La grande majorité d'entre eux sont libres et, à fortiori, gratuits. Ils vous seront d'une précieuse aide pour **rédiger**, **formater** ou **corriger** votre code. Certains d'entre eux, comme VS Code, possèdent un [terminal](https://fr.wikipedia.org/wiki/Interface_en_ligne_de_commande) et la [gestion de versions](https://fr.wikipedia.org/wiki/Gestion_de_versions) intégrés.

+ [Visual Studio Code](https://code.visualstudio.com/) est un éditeur de code, libre et gratuit. Il a l'avantage d'intégrer un terminal et un contrôle de version intégré (Git).
+ [codesandbox](https://codesandbox.io/) est un éditeur en ligne pour travailler avec toute une série de languages et de frameworks
+ L'[éditeur en ligne de P5js](https://editor.p5js.org/). Créez vous un compte (les yeux fermés) et vous pourrez sauver, partager et éditer vos sketches en ligne.
+ Atom.io : [https://atom.io/](https://atom.io/)
+ SublimeText : [https://www.sublimetext.com/](https://www.sublimetext.com/)

## Les tutoriels
+ [https://fr.flossmanuals.net/processing/](https://fr.flossmanuals.net/processing/) : la documentation **Processing** en français
+ Un très complet cours sur **P5js**, en français: [http://wiki.t-o-f.info/P5js/P5js](http://wiki.t-o-f.info/P5js/P5js)
+ [Introduction to **Computational Media** with p5.js](https://nycdoe-cs4all.github.io/)
+ [Introduction to **Programming for the Visual Arts** with p5.js](https://www.kadenze.com/courses/introduction-to-programming-for-the-visual-arts-with-p5-js/info)
+ Une série de cours à propos de **P5js** : [http://www.lyceelecorbusier.eu/p5js/?cat=2](http://www.lyceelecorbusier.eu/p5js/?cat=2)
+ Les **transformations** dans P5 (Gene Kogan) : [http://genekogan.com/code/p5js-transformations/](http://genekogan.com/code/p5js-transformations/)
+ Le **bruit de Perlin** dans P5 (Gene Kogan) : [http://genekogan.com/code/p5js-perlin-noise/](http://genekogan.com/code/p5js-perlin-noise/)
+ **Machine learning** et javascript: [ML5 by Dan Shiffman](https://www.youtube.com/watch?v=jmznx0Q1fP0)
+ **WebSockets**! : [https://www.youtube.com/watch?v=bjULmG8fqc8](https://www.youtube.com/watch?v=bjULmG8fqc8)
+ [**Manipuler l'information** (Open classrooms)](https://openclassrooms.com/fr/courses/3930076-manipuler-linformation)
+ **SVG** on the web : [https://svgontheweb.com/](https://svgontheweb.com/)
+ Une [série de tutoriels](https://www.youtube.com/playlist?list=PLKYvTRORAnx6a9tETvF95o35mykuysuOw) sur **Node-Red** (en anglais)


## Les bibliothèques

+ Les [bibliothèques pour étendre les possibilités de P5js](https://p5js.org/libraries/) (qui en est une également!)
+ **Vectoriel** > **[Paper.js](http://paperjs.org/examples/hit-testing/)** is an open source vector graphics scripting framework that runs on top of the HTML5 Canvas.
+ **3D** > **[Three.js](https://threejs.org/)** Discover: [https://discoverthreejs.com/book/introduction/](https://discoverthreejs.com/book/introduction/)
+ **Print** > **[Basil.js](http://basiljs.ch/about/)** est une bibliothèque javascript développée à l'[Ecole de Design de Bâle](http://thebaselschoolofdesign.ch/) pour faciliter l'utilisation de scripts personnalisés et de design génératif au coeur même d'[Adobe Indesign](https://fr.wikipedia.org/wiki/Adobe_InDesign). Il se définit comme *"An attempt to port the spirit of the Processing visualization language to Adobe Indesign."*

## Python

+ Python ex: [https://www.makeartwithpython.com/](https://www.makeartwithpython.com/)

## Showcase

+ P5js examples : [https://www.courses.tegabrain.com/CC17/basic-p5js-examples/](https://www.courses.tegabrain.com/CC17/basic-p5js-examples/)
+ Processing examples : [http://learningprocessing.com/examples/](http://learningprocessing.com/examples/)
+ Open Processing : [https://www.openprocessing.org/](https://www.openprocessing.org/)
+ Pour le plaisir : [https://www.youtube.com/watch?v=nvH2KYYJg-o](https://www.youtube.com/watch?v=nvH2KYYJg-o)
+ [Flow fields](https://www.bit-101.com/blog/2017/10/flow-fields-part-i/)

## Divers

+ [http://nickm.com/trope_tank/10_PRINT_121114.pdf](http://nickm.com/trope_tank/10_PRINT_121114.pdf)
