---
title: Adresses utiles
lang: fr-FR
---
# Adresses utiles

## Sites de référence
+ Le site de Processing : https://processing.org/
+ Le site de P5js : https://p5js.org/
+ Open Processing : https://www.openprocessing.org/
+ Le support des workshops : https://codeb3.netlify.com/
+ Atom.io : https://atom.io/
+ SublimeText : https://www.sublimetext.com/
+ Visual Studio Code : https://code.visualstudio.com/
+ Brackets : http://brackets.io/

## Adresses évoquées au cours

+ http://keycode.info/


## P5js, code : cours

+ http://wiki.t-o-f.info/P5js/P5js
+ Introduction à P5js : https://b2renger.github.io/Introduction_p5js/
+ [Introduction to Computational Media with p5.js](https://nycdoe-cs4all.github.io/)
+ Les transformations dans P5 (Gene Kogan) : http://genekogan.com/code/p5js-transformations/
+ Le bruit de Perlin dans P5 (Gene Kogan) : http://genekogan.com/code/p5js-perlin-noise/
+ WebSockets! : https://www.youtube.com/watch?v=bjULmG8fqc8
+ [Introduction to Programming for the Visual Arts with p5.js](https://www.kadenze.com/courses/introduction-to-programming-for-the-visual-arts-with-p5-js/info)
+ ML5 by Dan Shiffman : https://www.youtube.com/watch?v=jmznx0Q1fP0
+ P5js examples : https://www.courses.tegabrain.com/CC17/basic-p5js-examples/
+ Processing examples : http://learningprocessing.com/examples/
+ https://fr.flossmanuals.net/processing/ : la documentation Processing en français

## Divers

+ Python ex: [https://www.makeartwithpython.com/](https://www.makeartwithpython.com/)
+ [https://scratch.mit.edu/](https://scratch.mit.edu/) : le site de Scratch (programmation par blocs)
+ https://microbit.org/fr/ : le site du Micro:bit de la BBC
+ https://www.raspberrypi.org/ : le site du nano ordinateur
+ [Manipuler l'information (Open classrooms)](https://openclassrooms.com/fr/courses/3930076-manipuler-linformation)
+ SVG on the web : https://svgontheweb.com/
+ [Les pavages de Sébastien Truchet](https://www.google.com/search?q=pavages+truchet&tbm=isch&source=univ&sa=X&ved=2ahUKEwjsoez0vqLhAhXCsKQKHSNHCRQQsAR6BAgJEAE&biw=1617&bih=978)
+ Playing with pixels : http://playingwithpixels.gildasp.fr/
+ VueJS lists : https://vuejs.org/v2/guide/list.html
+ VueJS : https://www.vuemastery.com/courses/intro-to-vue-js/list-rendering/
+ Pour le plaisir : https://www.youtube.com/watch?v=nvH2KYYJg-o
+ Flow fields: https://www.bit-101.com/blog/2017/10/flow-fields-part-i/