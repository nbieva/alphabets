Ce que je vous conseille de revoir avant de poursuivre

Les éléments probablement els plus importants (et indispensables) à revoir avant de poursuivre sont les suivants:

+ Structure d'un programme: **setup** et **draw** (principes identiques pour p5*js et Processing)
+ Le **système de coordonnées** (identique pour p5*js et Processing)
+ Les **formes simples** (fonctions en général identiques pour p5*js et Processing)
+ La **couleur** (Remplissage, contour, etc..) (syntaxe identique pour p5*js et Processing)
+ Les **variables**
+ Les **conditions**
+ Les **boucles**


+ Designaknit
+ Image dithering