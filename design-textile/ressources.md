---
title: AlphaBits
lang: fr-FR
---

# Workshop PixelMania

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://player.vimeo.com/video/178215778?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

> [Purin Phanichphant, Instant Wallpaper Generator](http://purin.co/Instant-Wallpaper-Generator) - [Docs](https://www.instructables.com/id/Instant-Wallpaper-Generator/)

+ https://issuu.com/jpagecorrigan/docs/type-code_yeohyun-ahn
+ https://www.schultzschultz.com/free-works.html
+ https://www.behance.net/gallery/83059339/On-The-Tip-Of-My-Tongue-Semi-Generative-Layout-Design
+ https://hydra-editor.glitch.me/
+ [https://www.pinar-viola.com/printgallery/](https://www.pinar-viola.com/printgallery/)
+ 10PRINT= https://editor.p5js.org/codingtrain/sketches/ryxWYgmwX

# Plan de la journée

+ Introduction et rappels
+ 10print : décortiquage et mise en route. Notions mises en pratique: variables, boucles, couleur, attributs de forme et autres fonctions de formes, random, etc...
+ Résumé des transformations dans Processing et P5js
+ Couleur: modes de fusion
+ Créer une interface: principe et pas à pas (créer page)
+ Pixelliser une image pour la rendre compatible avec POint carré. Travailler les pixels d'une image: démo simple!

# Exercice

+ choisir une lettre et travailler à partir de celle-là pour explorer.
+ Choisir un mot (combinaison de lettres)

-----

# Textile

+ Le but est d'obtenir des résultats aujourd'hui mais surtout plus tard. C'est un outil supplémentaire.
+ Commencer en racontant l'histoire de 10 print ou comment exploiter déjà sans fin une seule ligne (pas besoin d'en faire des caisses...)
+ Le but n'est pas de tout coder mais de comprendre ce qui se passe, et de savoir jouer avec en modifiant les variables etc.
+ Faire la liste des éléments sur lesquels nous pouvons agir (couleur, opacité, random en détail, HSB, transformations...)
+ Tout le monde doit partager au moins un de ses motifs sur le web avec p5js. par groupe de deux?
+ On commence par un petit rappel du workshop B1
+ Créer une interface
+ Importer des éléments
+ Les faire tourner
+ Modifier leur échelle, agrandir, rapetissir
+ modifier la couleur (remplissage et contour)
+ Modifier l'opacité (remplissage et contour)
+ Ajouter un contour
+ Modifier l'épaisseur du contour
+ Modifier le mode de fusion (BlendMode)
+ Partager les résultats (envoyer l'URL à l'adresse email suivante : )

