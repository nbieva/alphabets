---
title: En pratique
lang: fr-FR
---


# Alphabits

+ Choisissez votre matière première (ttf)
+ analysez les caractères et supposez les plus intéressants, ou les paires ou groupes les plus intéressants, ou les mots ou phrases.
+ Soyez concis.