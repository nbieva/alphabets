---
title: De Fontstruct à Processing 
lang: fr-FR
---

# De Fontstruct à Processing...

La passerelle entre Fontstruct et Processing est le fichier ttf

En fonction des bibliothèques utilisées et de leurs version(minifiée ou non par exemple), les messages d'erreur seront plus ou mois explicites. En général, ils vous donnent au moins l'un ou l'autre indice sur ce qui pourrait poser problème et/ou le fichier et la ligne de votre code qui produit l'erreur.

## [Fontstruct](https://fontstruct.com/)

Fontstruct

## ... et vers [DesignaKnit](https://www.softbyte.co.uk/designaknit9.htm)

### Stitch Designer

Stitch Designer vous invite à concevoir des motifs de points sur une grille liée aux points, à travers un certain nombre de répétitions de motifs, à presque toutes les tensions, avec ou sans texture de tissu visible.

### Graphic Studio

Graphics Studio permet la conversion de fichiers d'images graphiques standard en motifs de points DesignaKnit. Des photographies, des numérisations, des captures d'écran d'autres logiciels et des graphismes peuvent tous être utilisés.

Il est possible de contrôler exactement comment les couleurs de l'image sont converties en couleurs de fil. Les couleurs de fil peuvent être choisies sur l'image - ou n'importe quelle nuance peut être spécifiée. Le nombre de points et de lignes et le nombre de couleurs de fil par ligne peuvent aussi être spécifiés.

## Diffusion d'erreur (Image Dithering)

## Créez vos modèles dans Photoshop ou Illustrator