---
title: Fonctions pour le texte dans Processing 
lang: fr-FR
---

# Fonctions pour le texte dans Processing

Vous trouverez ci-dessous un aperçu des fonctions principales pour travailler le texte dans Processing, qu'il s'agisse d'importer une police de caractère ou des différents paramètres disponibles pour la gestion du texte dans Processing (textSize, textWidth, textAlign, textLeading, ...).

## La variable PFont

Au même titre que d'autre type de variables comme *int*, *float* ou *color*, Processing possède un type de variable spécifique pour stocker les polices de caractères. Ce type est **PFont**.

## Polices Vetorielles et polices Pixels

Vous 

### Le format vlw

Le format **vlw** est un format issu d'un workshop du MIT media Lab.

The vlw file extension is associated with Processing.

The vlw file contains font that can be inserted to Processing images and animations.

## Caractère par caractère

```processing
// Code from Dan Shiffman -> https://processing.org/tutorials/text/
PFont f;
String message = "Each character is written individually.";

void setup() {
  size(400, 150);
  f = createFont("Arial",20,true);
}

void draw() { 
  background(255);
  fill(0);
  textFont(f);         
  int x = 10;
  for (int i = 0; i < message.length(); i++) {
    textSize(random(12,36));
    text(message.charAt(i),x,height/2);
    // textWidth() spaces the characters out properly.
    x += textWidth(message.charAt(i)); 
  }
  noLoop();
}
```