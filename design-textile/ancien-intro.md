---
title: Ancien intro
lang: fr-FR
---

<div class="lead">
<p>
<strong>Why did you use this tools?</strong><br/>
There was no other way to accomplish my task.</p>
<p style="font-size:13px;"><a href="http://emohr.com/" target="_blank">Manfred Mohr</a> (interview dans  <a href="https://www.amazon.fr/Processing-Programming-Handbook-Designers-Artists/dp/026202828X/ref=sr_1_1?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=Processing%2C+A+programming+Handbook+for+Visual+Designers+and+Artists&qid=1572774233&sr=8-1" target="_blank">ce livre</a>)</p>
</div>

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://player.vimeo.com/video/178215778?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

> [Purin Phanichphant, Instant Wallpaper Generator](http://purin.co/Instant-Wallpaper-Generator) - [Docs](https://www.instructables.com/id/Instant-Wallpaper-Generator/)

# AlphaBits

Dans la continuité du workshop Processing auquel vous avez participé en B1, ce module d'une journée va vous permettre d'envisager le code comme outil de recherche graphique, et plus spécifiquement pour la **création de motifs**. Bien évidemment, nous nous baserons sur les fontes que vous avez produites et dont j'ai pu me procurer l'un ou l'autre échantillon. Nous verrons comment les charger dans un sketch et comment le code peut nous permettre de les modifier à la volée, et ce directement dans la page web.

Nous verrons ensuite comment créer une interface simple qui nous permettent de manipuler nos variables dans le navigateur, de façon plus conviviale. En somme, nous allons contruire notre propre outil de recherche graphique, adapté à nos besoins spécifiques.

+ Exploitation de vos polices réalisées dans Fontstruct pour produire une série de motifs pour la production de tissus, trames, dans DesignaKnit. + Recherches visuelles libres
+ Plus largement, expérimenter l'intégration du code à vos outils de **recherche, production et diffusion artistique**. Vos deux ateliers sont particulièrement concernés par ce point.
+ Vous donner une série de **ressources**
+ Créer un espace d'**échange et d'expérimentation** autour du code et de la programmation. 

Ce dernier point aurait pris tout son sens dans une journée de workshop "physique", dont nous sommes malheureusement privés.

Nous jeterons un oeil, un peu à l'aveugle vu la situation, au logiciel **Designaknit**, et verrons comment y importer nos motifs. (on tentera)

Enfin, nous verrons comment **partager et diffuser notre travail** sur le web, qu'il s'agisse de notre code source ou de nos productions visuelles.

## Nos outils

J'ai pensé un temps travailler avec **Processing** (ce que nous pourrions faire ou ferons aussi..) mais je vais passer assez rapidement à une bibliothèque javascript appelée **P5*js** qui nous permettra d'exécuter notre code directement dans nos pages web et offre ainsi de bien plus intéressantes possibilités pour ce qui est de la diffusion et du partage de notre travail. Cela nous permettra aussi de mettre en pratique les langages et outils abordés dans vos cours de Web B2.

A notre niveau, 95% de ce que nous avons vu avec Processing est transposable directement dans P5*js, qu'il s'agisse de la syntaxe du langage ou des concepts logiques abordés.

Mais nous verrons bien évidement, conrètement, comment passer d'un sketch Processing à un sketch P5*js et inversément.

#### La journée comprendra trois parties principales:

1. La première, établira une base théorique de la programmation en revoyant ses **notions de base** à l'aide de Processing ou P5*js. Nous reparlerons de dessin, de variables, de boucles, mais évoquerons aussi les tableaux.
2. La deuxième partie se focalisera plus particulièrement sur le texte et la création de motifs sur base des polices créées auparavant.
3. La troisième mettra l'accent sur l'export de fichiers et le partage de ceux-ci via le web.

## Processing et P5js

[Processing](https://www.processing.org/) est un logiciel libre, créé par des artistes, pour des artistes, dans les champs pédagogique et visuel. 
Très visuel lui-même, Processing permet d'entrer facilement dans les **logiques d'un algorithme** et d'en comprendre les fonctions de base (variables, boucles, conditions, fonctions, listes..)

[P5js](https://p5js.org/) est une bibliothèque javaScript en filiation directe avec Processing, et qui nous permettra d'interagir avec la page web et de partager nos travaux plus facilement.

> Plus d'info sur [Processing & P5js](generalites/processing-et-p5js.md)