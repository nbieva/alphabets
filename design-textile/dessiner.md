---
title: Dessiner avec la lettre 
lang: fr-FR
---

# Dessiner avec la lettre 

La console est un outil de votre navigateur vous permettant de résoudre les erreurs éventuelles de votre codee javascript. Vous pouvez également y vérifier l'état de certaines variables.

En fonction des bibliothèques utilisées et de leurs version(minifiée ou non par exemple), les messages d'erreur seront plus ou mois explicites. En général, ils vous donnent au moins l'un ou l'autre indice sur ce qui pourrait poser problème et/ou le fichier et la ligne de votre code qui produit l'erreur.
