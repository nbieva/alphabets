---
title: Motifs
lang: fr-FR
---

# Motifs

Quelques méthodes pour créer un motif avec Processing

+ Bloc par bloc
+ Créer la fonctions de votre bloc (pour pouvoir l'isoler à la fin)
+ La répéter
+ Bloc par bloc avec variations (interactions, couleurs...)
+ Image par image
+ Avec interface
+ Exporter votre bloc de base
+ Exporter l'image entière
+ Exporter des séquences entières
+ En faire un film.
  