# socket.io

+ creer le fichier html
+ créer styles.scss

Si SASS n'est pas encore installé sur le système
```bash
npm install -g node-sass 
```

```bash
npm init 
```

Dans l'objet scripts de package.json :

```json
"scss": "node-sass --watch assets/scss -o assets/css"
```

Lancer la commande
```bash
npm run scss //ou yarn scss
```

Maintenant que le HTML et SASS sont en places...

Installer vue
```bash
npm install -g @vue/cli
```

Ajouter sudo devant la commande si nécessaire

https://www.youtube.com/watch?v=lcYn0tgUvHE

yarn serve