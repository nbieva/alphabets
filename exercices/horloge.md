---
title: Créez une horloge
lang: fr-FR
---

# Créez une horloge

Processing et P5js ont tous deux quelques variables natives liées au temps et permettant de récupérer en temps réel l'heure ( **hour()** ), la minute ( **minute()** ), la seconde ( **second()** ) et la milliseconde ( **millis()** ).

A partir de là, vous pouvez tout imaginer pour créer une sorte d'horloge pour visualiser le temps qui passe d'une façon inédite. Cela peut-être créer un sketch purement visuel, ou manipuler les valeurs numériques (ce que renvoient ces fonctions) pour brouiller les pistes, etc..

Vous trouverez ci-dessous un code d'exemple (qui n'est certainement pas un modèle) avec ces différentes variables. Comme vous le voyez, le code pour Processing et P5js est assez semblable. Préférez P5js si vous prévoyez un partage sur le web.

Cette page reprend les quelques différences entre Processing et P5js pour passer de l'un à l'autre (ou vice-versa)

<iframe class="p5embed" width="740" height="310" src="https://editor.p5js.org/nicobiev/embed/cWneuXsNd"></iframe>

## Avec Processing

```processing
void setup() {
  size(800,600);
  noStroke();
}

void draw() {
  background(0, 122, 142);
  fill(76, 165, 71);
  rect(0,0,hour()*(width/24),height/3);
  fill(61, 121, 87);
  rect(0,height/3,minute()*(width/60),height/3);
  fill(64, 94, 104);
  rect(0,2*(height/3),second()*(width/60),height/3);
}
```
Ou
```processing
void setup() {
  size(800,600);
  noStroke();
  colorMode(HSB,60,100,100);
}
void draw() {
  background((hour()/24)*60, 100, 100);
  fill(minute(),100,100);
  rect(50,50,width-100,height-100, 10);
  fill(second(),100,100);
  rect(150,150,width-300,height-300, 10);
}
```

## Avec P5js

```javascript
function setup() {
  createCanvas(windowWidth,windowHeight); // mais pourrait aussi être 800,600
  noStroke();
}

function draw() {
  background(0, 122, 142);
  fill(76, 165, 71);
  rect(0,0,hour()*(width/24),height/3);
  fill(61, 121, 87);
  rect(0,height/3,minute()*(width/60),height/3);
  fill(64, 94, 104);
  rect(0,2*(height/3),second()*(width/60),height/3);
}
```