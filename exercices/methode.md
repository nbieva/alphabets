---
title: Méthode
lang: fr-FR
---

# But

+ Vous faire dévouvrir l'intérêt complémentaire de ces outils
+ Deux parties à la journée: Processing et P5js

# Méthode

+ Je vais vous lancer beaucoup de pistes
+ Puis on va essayer de les pousser le plus loin possible
+ Mon support sera limité au long de la journée

## Théorie

+ https://processing.org/tutorials/transform2d/
+ https://happycoding.io/tutorials/processing/input#cheat-sheet
+ P5js
+ Fonctions personnalisées
+ P5js
+ Liste de fonctions
+ Les tableaux
+ **key** variable

## Pistes

+ Réfléchir à ce que l'on veut faire
+ Dessiner avec une lettre, puis quand on appuie sur une touche, placer des lettres-raccords sur les coins ou les bordures.
+ Commencer par 10 print
+ Réfléchissez à ce que vous voulez réaliser et commencez par un croquis
+ Commencez par des choses très simples
+ Montez ensuite en complexité
+ Sauvegardez ou exportez vos étapes
+ Documentez votre travail
+ Partir d'un pattern graphique et l'adapter à la lettre (**A la lettre**...)
+ Créer une horloge de texte
+ Transformation 0 45 90 135 etc.. degrés

## Fonctions utiles

+ key
+ keyCode
+ createFont

## Exercices

# XR6 1: travail avec une lettre

+ Choisissez une lettre et épuisez là

# XR6 2: 10 PRINT avec une lettre

+ 10 PRINT avec une lettre
+ Créer un programme qui passe tout l'alphabet en revue.. avec la touche key
+ Créer un programme qui répète un motif
+ Créer une interface

# XR6: Générer des phrases (cadavres exquis)

+ Définissez une série de mots/verbes que vous combinez avec des tableaux

# XR6: Construire une interface clavier

+ Choisissez une lettre et épuisez-la
+ Changez couleurs, taille, contour et autres paramètres

# Rendu

+ Production de 5 motifs avec pour chacun le motif isolé et une page web représentant l'ensemble, avec une capture d'écran et l'URL du travail

