---
title: Contenu du workshop
lang: fr-FR
---

<div class="lead">
<p>
<strong>Why did you use this tools?</strong><br/>
There was no other way to accomplish my task.</p>
<p style="font-size:13px;"><a href="http://emohr.com/" target="_blank">Manfred Mohr</a> (interview dans  <a href="https://www.amazon.fr/Processing-Programming-Handbook-Designers-Artists/dp/026202828X/ref=sr_1_1?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=Processing%2C+A+programming+Handbook+for+Visual+Designers+and+Artists&qid=1572774233&sr=8-1" target="_blank">ce livre</a>)</p>
</div>

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem;"><iframe src="https://player.vimeo.com/video/178215778?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

> [Purin Phanichphant, Instant Wallpaper Generator](http://purin.co/Instant-Wallpaper-Generator) - [Docs](https://www.instructables.com/id/Instant-Wallpaper-Generator/)

# Contenu du workshop

Bienvenue à toutes et tous,

Dans la continuité du workshop Processing auquel vous avez participé en B1, ce module d'une journée va vous permettre d'envisager le code comme outil de recherche graphique, et plus spécifiquement pour la **création de motifs à partir des fontes que vous avez produites** et dont j'ai reçu l'un ou l'autre échantillon. Nous verrons comment les charger dans un sketch et comment le code peut nous permettre de les modifier à la volée, et ce directement dans la page web.

Nous travaillerons pour ceci avec une bibliothèque javascript appelée [P5js](https://p5js.org/), vous permettant d'exécuter votre code directement dans un navigateur, sans avoir rien d'autre à installer. Comme vous êtes à présent au courant de ce que sont les langages HTML et CSS, vous devriez pouvoir vous y retrouver.

Nous verrons ensuite comment **créer une interface simple** qui nous permette de manipuler nos variables dans le navigateur, de façon plus conviviale. En somme, nous allons contruire notre propre outil de recherche graphique, adapté à nos besoins spécifiques.

**Cette page sera votre référence pour ce workshop.** Vous y trouverez les **liens vers les différentes vidéos**. Le reste du site, dont une partie du contenu est partagé avec les B1 et les B3, reste à votre disposition en guise de rappel ou de référence. Je pense à certaines pages qui pourraient particulièrement vous intéresser, comme celle traitant de l'[interactivité clavier](/en-pratique/interactivite-clavier) et [souris](/en-pratique/interactivite-souris).

Parcourez également les **quelques exemples ou liens** que vous trouverez sur ces pages: [filiation](/generalites/filiation) et [exemples](/generalites/inspirations). La page a été mise à jour en fonction de notre workshop.

<iframe class="p5embed" width="740" height="370" src="https://editor.p5js.org/nicobiev/embed/p0BrzZ9R"></iframe>

**(Cliquez sur l'image ci-dessus et taper sur une touche de votre clavier..)**

N'hésitez pas à trouver une façon de travailler en groupe, ou faites le point de temps en temps ensemble. C'est souvent plus sympa. N'oubliez pas que, via votre adresse mail liée à l'école, vous avez accès à des outils comme Google Hangouts Meet pour travailler ensemble, éventuellement avec partage d'écran.

De façon générale, essayez d'épuiser les possibilités de l'outil, de ne pas rester en surface. Les vidéos et les exemples évoqués ne sont là que pour vous montrer comment les choses fonctionnent ou sont construites. Il y a moyen d'aller **BIEN PLUS LOIN** avec les différentes choses que nous utiliserons. Avec peu, on peut faire beaucoup.

Les deux principales propositions de travail sont reprises ci-dessous. Cela dit, n'hésitez pas à créer vos propres sketches. Je mets également bien évidemment une série de vidéos sur l'**import de votre police de caractère dans votre projet**, et d'autres sur les **boucles et boucles imbriquées**, qui sont au coeur de notre deuxième outil de recherche, Patternator (vous l'appelerez comme vous le voulez! Je suis ouvert à toutes les propositions ;) ).

Voici le lien vers la totalité de la playlist: [https://www.youtube.com/playlist?list=PLDdoBMCKjS0azfUz23xvoyHO7uTnnACrr](https://www.youtube.com/playlist?list=PLDdoBMCKjS0azfUz23xvoyHO7uTnnACrr)

Mais les liens vers **les différentes vidéos sont repris ci-dessous (chaque fois en gras)**.

Bon travail.

## Introduction

+ **Introduction générale: [https://youtu.be/DOXSpQafDHQ](https://youtu.be/DOXSpQafDHQ)**
+ **Présentation rapide des exercices: [https://youtu.be/Q2W4az5YTNY](https://youtu.be/Q2W4az5YTNY)**
+ **Manfred Mohr et Purin Phanichphant: [https://youtu.be/eTrBf-N1SlE](https://youtu.be/eTrBf-N1SlE)**

## Retour sur Processing et présentation de P5js

+ **Retour sur Processing et présentation de P5js: [https://youtu.be/GtkoVoy75SE](https://youtu.be/GtkoVoy75SE)**
+ **N'hésitez pas à piocher à droite à gauche sur ce site pour vous rafraîchir la mémoire: [https://youtu.be/VN4H_01W6rA](https://youtu.be/VN4H_01W6rA)**
+ **L'éditeur de P5js: [https://youtu.be/6jbPtM_I8wY](https://youtu.be/6jbPtM_I8wY)**

## 10print

[10print](https://10print.org/) est une base de travail très intéressante pour la création de motifs. Deux éléments (ici slash et antislash) sont affichés alternativement, et ce de façon aléatoire (je ne sais pas si alternatif et aléatoire sont compatibles mais vous m'aurez compris..).

<a data-fancybox title="" href="/assets/10print.png">![](/assets/10print.png)</a>

### Les différentes vidéos traitant de la chose:

+ **Présentation du travail: [https://youtu.be/7ZdS0qOQXkI](https://youtu.be/7ZdS0qOQXkI)**
+ **Première partie du code: [https://youtu.be/q3uRSUBa8h4](https://youtu.be/q3uRSUBa8h4)**
+ **Finalisation et variantes sur le même thème: [https://youtu.be/MaScm9rNZcM](https://youtu.be/MaScm9rNZcM)**

Vous pourriez, à l'aide des boucles for() imbriquées, créer une variante de ceci qui, au lieu de dessiner les éléments un par un, remplir tout l'espace de travail (votre canvas) en un seul passage. (voyez la vidéo sur les boucles pour ça..)

Notez que cet exercice n'est qu'une base. Une fois la logique mise en place, à vous de l'enrichir en **modifiant les variables graphiques**, en modifiant les éléments qui se répètent, etc.. En effet, nous ne sommes pas obligés d'alterner aléatoirement entre un slash et un anti-slash. Nous pourrions tout aussi bien alterner entre un cercle rouge et un carré bleu (l'exemple n'est pas très original.. A vous d'en imaginer de plus intéressants...), ou tout autre élément.

### En pratique:

Essayez de créer 3 visuels sur la base de ce code, en le modifiant comme bon vous semble. Exportez-les en intégrant la fonction **save()** à votre code (voir ci-dessous) ou en faisant une capture d'écran à la main, et envoyez-les moi par email sur nicolas.bieva@lacambre.be (nommez vos fichiers avec vos nom et prénom).

### Liens intéressants

+ Le site de 10print.org: [https://10print.org/](https://10print.org/)
+ Sur les pavages de Truchet: [https://pelletierauger.com/fr/projets/les-pavages-de-truchet.html](https://pelletierauger.com/fr/projets/les-pavages-de-truchet.html)
+ Encore une: [https://images.math.cnrs.fr/Les-pavages-de-Truchet.html](https://images.math.cnrs.fr/Les-pavages-de-Truchet.html)
+ Une demo: [https://editor.p5js.org/nicobiev/present/YyKwIMYk1](https://editor.p5js.org/nicobiev/present/YyKwIMYk1)
+ Une autre façon de s'y prendre pour obtenir un résultat semblable: [https://editor.p5js.org/codingtrain/sketches/ryxWYgmwX](https://editor.p5js.org/codingtrain/sketches/ryxWYgmwX)
+ La fonction line: [https://p5js.org/reference/#/p5/line](https://p5js.org/reference/#/p5/line)
+ Mais encore.. ce site où vous trouverez une foule d'emple de code intéressants: [http://www.generative-gestaltung.de/2/](http://www.generative-gestaltung.de/2/)

## Charger votre police et capter le clavier

+ **Mise en place d'un projet web local avec [VSCode](https://code.visualstudio.com/) (plutôt que dans l'éditeur de P5js). Certains préféreront cette façon de faire. Chacune a ses avantages et ses inconvénients: [https://youtu.be/PwPcerwgtNc](https://youtu.be/PwPcerwgtNc)**
+ **Charger votre police dans votre sketch: [https://youtu.be/Mti8Is4YQjQ](https://youtu.be/Mti8Is4YQjQ)**
+ **Capter l'événement clavier: [https://youtu.be/-1bt35889E0](https://youtu.be/-1bt35889E0)**

## Les boucles et boucles imbriquées

Ces 2-3 vidéos reviennent sur les boucles, qui sont à la base de la petite interface qui suit (le patternator)

+ **Présentation rapide d'une partie du code et des logiques derrière l'interface du Patternator: [https://youtu.be/sxb-o89FGNE](https://youtu.be/sxb-o89FGNE)**
+ **Le principes des boucles for(), en essayant d'être clair: [https://youtu.be/f87sQHf61-M](https://youtu.be/f87sQHf61-M)**
+ **Les boucles imbriquées: [https://youtu.be/QFiiP99pBUo](https://youtu.be/QFiiP99pBUo)**
+ **Cette vidéo-ci ne me semble pas claire du tout.. C'est une vidéo de fin de journée ;) Essayez d'y prendre ce que vous pouvez. Laissez le reste ;) : [https://youtu.be/7ZerP6c-Pgk](https://youtu.be/7ZerP6c-Pgk)**

## Patternator

Cette petite interface est un bon exemple (tout à fait imparfait et inachevé..) de ce que l'on peut réaliser avec les outils avec lesquels vous êtes à présent un petit peu familiers, à savoir HTML, CSS et JS.

+ **Le principe et l'interface du Patternator (comme je l'ai déjà dit, votez pour un autre nom, je serai ravi!) : [https://youtu.be/ABNzbBz9djc](https://youtu.be/ABNzbBz9djc)**

Vous pouvez faire tourner l'application localement sur votre ordinateur (Je vous le conseille!). le cas échéant, vous pourrez la trouver ici: [https://patternator.netlify.app/](https://patternator.netlify.app/)

Si vous en faites une autre version (on ne sait jamais..), mettez en ligne et partagez!

<a data-fancybox title="" href="/assets/patternator.png">![](/assets/patternator.png)</a>

### En pratique:

1. [Téléchargez le dossier du projet ici](/assets/patternator.zip)
2. Trouvez comment vous y prendre pour y intégrer votre propre police de caractères.
3. Familiarisez-vous avec l'interface
4. Exportez 5 motifs qui vous semblent particulièrement intéressants (exportez chaque fois la base du motif + le canvas). Si vous le pouvez, essayez de formuler ce qui vous intéresse dans ces motifs.
5. Donnez-leur un petit nom
6. Rassemblez vos fichiers dans un dossier et organisez-les.
7. Zippez (compressez) ce dossier et envoyez-le moi par email sur nicolas.bieva@lacambre.be

## Exporter votre canvas

Dans P5js, un peu comme dans Processing, vous pouvez exporter le canvas sous forme de fichier JPG par exemple. Le tout est de savoir formuler à quel moment (quand on presse une touche? ou toutes les 20 secondes? ou à 16 heures précises? etc..) et de générer des noms de fichiers différents pour éviter qu'ils ne s'écrasent les uns les autres.

+ [https://p5js.org/reference/#/p5/save](https://p5js.org/reference/#/p5/save)

J'utilise parfois le code ci-dessous pour exporter une image lorsque je presse la touche "s" (à ne pas mettre dans le draw!!):

```javascript
// Cette fonction doit se mettre au même niveau que le setup ou le draw
function keyTyped() {
  if (key === 's') {
    save("monimage-"+hour()+"-"+minute()+"-"+second()+".jpg");
  } 
}
```

Bon travail!

Si vous avez besoin d'aide, faites signe!