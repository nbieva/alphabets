---
title: Console et gestion des erreurs 
lang: fr-FR
---

# Console et gestion des erreurs

La console est un outil de votre navigateur vous permettant de résoudre les erreurs éventuelles de votre codee javascript. Vous pouvez également y vérifier l'état de certaines variables.

En fonction des bibliothèques utilisées et de leurs version(minifiée ou non par exemple), les messages d'erreur seront plus ou mois explicites. En général, ils vous donnent au moins l'un ou l'autre indice sur ce qui pourrait poser problème et/ou le fichier et la ligne de votre code qui produit l'erreur.

La fonction **print("Hello");** envoie un message dans la console. Identique à la fonction **console.log("Hello");**.

```javascript
// Cette fontion peut s'exécuter directement dans votre console
console.log("Less is more");
```

```javascript
a = 2;
banane = 3;
a + banane // Renverra la valeur 5
```

<a data-fancybox title="Bug" href="/assets/bug.jpg">![Bug](/assets/bug.jpg)</a>
<span class="legende">Photo du « premier cas réel de bug (insecte) », dans le journal d'entretien du Harvard Mark II conservé à la Smithsonian Institution.</span>

# Inspecteurs web

Probablement évoqués en B2, les outils de développement des navigateurs vous seront vite indispensables.

Les outils de développement dans Safari doivent être activés. Ils ne le sont pas par défaut.

+ [Outils de développement de Firefox](https://developer.mozilla.org/fr/docs/Outils)
+ [Inspecteur Firefox](https://developer.mozilla.org/fr/docs/Outils/Inspecteur)
+ [https://developer.mozilla.org/fr/docs/Apprendre/D%C3%A9couvrir_outils_d%C3%A9veloppement_navigateurs](https://developer.mozilla.org/fr/docs/Apprendre/D%C3%A9couvrir_outils_d%C3%A9veloppement_navigateurs)

<a data-fancybox title="Bug" href="/assets/inspecteur1.png">![Bug](/assets/inspecteur1.png)</a>
<span class="legende">L'inspecteur web du navigateur [Brave](https://brave.com/fr/)</span>

Il y a quelques années maintenant, nous avions la possibilité d'accéder à une **vue 3D** de notre page web via l'inspecteur de Firefox. Cette fonctionnalité a malheureusement été supprimée. 

<a data-fancybox title="Bug" href="/assets/tilt.png">![Bug](/assets/tilt.png)</a>

# Méthode

Préférez partir d'un projet simple pour ensuite monter en complexité. Cela vous permettra de ne jamais totalement perdre pied, et d'identifier aisément les concepts ou fonctions que vous ne comprenez pas.

C'est exactement ce que décrivent Casey Reas, Ben Fry et Lauren MacCarthy:

+ Nous recommendons d'**écrire votre code par petits blocs** de quelques lignes et de l'exécuter régulièrement pour éviter que les erreurs ne s'accumulent à votre insu.
+ Même les programmes les plus ambitieux sont écrits ligne par ligne.
+ **Divisez votre projet en unités plus petites** que vous achevez l'une après l'autre. cela vous permettra d'accumuler une série de petits succès plutôt qu'une vague d'erreurs.
+ Si vous rencontrez une erreur, essayez d'**isoler** la portion de votre code où vous pensez la trouver.
+ Essayez d'envisager la résolution d'une erreur comme celle d'un mystère ou d'un puzzle. Si vous êtes bloqué ou que vous perdez patience, **faites une pause** pour vous vider la tête ou demandez de l'aide à un ami.
+ Souvent, l'erreur est juste sous notre nez mais nécessite un avis extérieur pour être mise en lumière.

Source: Make: [Getting started with P5.js by Lauren McCarthy, Casey Reas and Ben Fry](https://www.amazon.fr/Processing-Programming-Handbook-Designers-Artists/dp/026202828X/)