---
title: La ligne de commande
lang: fr-FR
---

## Interface en ligne de commande

<img src="/assets/terminal1.png" style="float:right;width:150px;margin:0 0 1rem 1.5rem;">Une interface en ligne de commande (en anglais command line interface, couramment abrégé CLI) est une interface homme-machine dans laquelle la **communication entre l'utilisateur et l'ordinateur** s'effectue **en mode texte** :

+ l'utilisateur tape une ligne de commande, c'est-à-dire du texte au clavier pour demander à l'ordinateur d'effectuer une opération ;
+ l'ordinateur affiche du texte correspondant au résultat de l'exécution des commandes tapées ou à des questions qu'un logiciel pose à l'utilisateur.

Source: [Wikipedia](https://fr.wikipedia.org/wiki/Interface_en_ligne_de_commande)

::: tip
#### Liens utiles
+ [Les bases sur le site du cours de Tega Brain](https://www.courses.tegabrain.com/cc18/github-and-command-line-basics/)
+ [Intro à la ligne de commande par Dan Shiffman](https://www.youtube.com/watch?v=oK8EvVeVltE)
+ [Le terminal dans Open classrooms](https://openclassrooms.com/fr/courses/638097-domptez-votre-mac-avec-os-x-mavericks/638095-le-terminal-dans-os-x)
+ [10 commandes utiles](https://computers.tutsplus.com/fr/tutorials/10-terminal-commands-that-every-mac-user-should-know--mac-4825)
:::

La ligne de commande peut paraître un peu nébuleuse de prime abord (et elle l'est sous certains aspects) mais elle est dans bien des cas **indispensable**, et l'on éprouve un certain plaisir à dialoguer avec la machine **sans interface graphique**. Les commandes tapées s'exécutent souvent bien plus vite qu'on ne pourrait l'imaginer et certaines de ces commandes nous permettent de **faire des choses impossibles autrement**. En bref, il faut savoir ce que c'est, et comment l'utiliser, sans pour autant en être un expert.

<a data-fancybox title="Le terminal" href="/assets/terminal.png">![Terminal](/assets/terminal.png)</a>

Vous croiserez régulièrement dans des tutoriels des commandes à exécuter dans votre terminal.

## Commandes utiles
 
**Afficher le répertoire courant** (Print working directory)
```bash
pwd
```

**Créer un répertoire** (Make directory)
```bash
mkdir monrepertoire
```

**Changer de répertoire** (Change directory) Le chemin d'accès vers le répertoire est ici très important. Sur mac, vous pouvez glisser un répertoire du finder vers le terminal pour afficher automatiquement le chemin d'accès.
```bash
cd monrepertoire
```

**Lister les fichiers et dossiers du répertoire courant**
```bash
ls
```

**Supprimer un répertoire (vide) ou un fichier** (Remove)
```bash
rm
```

**Afficher les fichiers cachés dans le Finder**
```bash
defaults write com.apple.finder AppleShowAllFiles TRUE
```

**Changer le format des captures d'écran**
```bash
defaults write com.apple.screencapture type file-extension
```

**Faites parler votre mac**
```bash
say vive les b3 qui m'entendent et m'écoutent
```

------------

<img src="https://i.pinimg.com/originals/5e/50/e3/5e50e3c0ee4bb967610271e4e43862c3.png" style="float:left;margin:0 2rem  1rem 0;">Il vous faudra ajouter le mot clé **sudo** si votre commande nécessite un accès administrateur.