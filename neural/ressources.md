---
title: Neural networks
lang: fr-FR
---

# Machine learning

+ Teachable machine: https://www.youtube.com/watch?v=kwcillcWOg0

+ https://runwayml.com/
+ https://www.tensorflow.org/js
+ https://teachablemachine.withgoogle.com/
+ https://experiments.withgoogle.com/teachable-machine
+ Machine learning : https://ml5js.org/
+ http://www.memo.tv/works/#selected-works