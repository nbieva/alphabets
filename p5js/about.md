---
title: A propos de P5js
lang: fr-FR
---

# A propos de P5js

P5js est une **bibliothèque JavaScript** -Open Source- créée par Lauren McCarthy en 2013. Elle descend (la bibliothèque, pas Lauren) directement de Processing et a été développée dans le même esprit, et avec les mêmes objectifs, mais adaptés au web d'aujourd'hui. Elle propose également un [éditeur en ligne](https://editor.p5js.org/) qui rend la collaboration et le partage très simples sur un projet. Pour sauver vos projets créés dans l'éditeur de p5js, vous devrez créer un compte (Allez-y les yeux fermés. Ils ne vous spammeront pas).

<div style="text-align:center;margin:30px 0;">

<a data-fancybox title="p5js" href="/assets/p5schema.png">![Boucles](/assets/p5schema.png)</a>

</div>

## Quand utiliser P5js? Quand utiliser Processing?

Votre projet vous imposera, d'une certaine manière, l'une ou l'autre. En règle générale retenez que vous devriez utiliser Processing si:

+ output static. pas d'interaction. Pas de partage (à grande échelle) sur le web.
+ Besoin de solliciter une grande puissance de calcul.
+ Le partage du processus de production n'offre aucun intérêt.

## Principales différences entre P5js et Processing

[Cette page](http://gerard.paresys.free.fr/Methodes/Methode-Processing-p5.html) reprend de façon plus exhaustive les différences entre Processing et P5js ainsi que la façon de passer de l'un à l'autre.

+ [http://gerard.paresys.free.fr/Methodes/Methode-Processing-p5.html](http://gerard.paresys.free.fr/Methodes/Methode-Processing-p5.html)

+ [Convertisseur en ligne](http://faculty.purchase.edu/joseph.mckay/p5jsconverter.html)


