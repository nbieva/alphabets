---
title: Retour sur Processing
lang: fr-FR
---

## Retour sur Processing

<p class="lead">Nous reviendrons ici sur <strong>Processing et les notions de base</strong> abordées lors du <a href="https://codeb1.netlify.com/" target="_blank">workshop B1</a>. Nous évoquerons également rapidement les différents <strong>outils</strong> et méthodes utilisés lorsque l'on travaille avec du code.</p>

<div style="padding:56.25% 0 0 0;position:relative;margin:2rem 0;border-radius:.4rem !important;">
<iframe src="https://player.vimeo.com/video/88903141?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:.4rem !important;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
</div>
<script src="https://player.vimeo.com/api/player.js"></script>

> [Fase (Four Movements to the Music of Steve Reich) by Anne Teresa De Keersmaeker, 1982. [part 3 of 4 - Violin phase]](https://vimeo.com/88903141)

+ [Processing et P5js](/generalites/processing-et-p5js.html) + satellites
+ Interface, Console, Syntaxe, commentaires etc..
+ [Structure de base d'un sketch](/processing/structure): Setup() et draw()
+ Parallèle avec [Scratch](https://scratch.mit.edu/projects/editor/) ou [Micro:bit](https://makecode.microbit.org/#editor), où le setup et le draw sont bien visibles.
+ [Système de  coordonnées](/processing/coordonnees), formes (rect, ellipse, line, etc..) - [Repères](http://mousehover.net/coordonnees/)
+ [couleur et modes colorimétriques](/processing/couleur)
+ Mode HSB - [schéma](https://p5js.org/assets/learn/color/hsb.png) - [Color selector](https://www.google.com/search?q=color+selector&oq=color+selector&aqs=chrome..69i57.4055j0j7&sourceid=chrome&ie=UTF-8). On pourra, par exemple, utiliser ces fonctions pour générer des CSS à la volée avec P5js.
+ Les variables natives + Création et utilisation de variables + variables booleennes
+ La fonction **random()**

### Fonctions évoquées:

+ rect(), ellipse(), noStroke(), stroke(), fill(), noFill(), rectMode(), ellipseMode(), random(), background(), size(), blendMode(), frameRate(), .. 
+ Variables natives: mouseX, mouseY, frameCount, width, heigth, ..

### Adresses utiles

Vous trouverez une série d'adresses utiles, de références et de tutoriels sur [cette page](/generalites/outils.html). N'hésitez pas à faire des suggestions si vous tombez sur une pépite...

### Ce que vous devriez savoir faire:

+ **Facilement:** Recréer [ce SVG(https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics#/media/Fichier:ExempleSimple.svg)] avec Processing (voir [ici](https://fr.wikipedia.org/wiki/Scalable_Vector_Graphics))
+ **Avec plus d'efforts (1):** En utilisant les **formes simples** et leurs paramètres (**couleur**, contour, etc..) et en utilisant un maximum de **variables**, créez un générateur de visuels que vous devez ensuite pouvoir **exporter** en PNG ou PDF en appuyant sur une touche du clavier.



## Un exemple JS (avec P5js)

```javascript
function setup() {
    // Taille de mon élément <canvas>
    createCanvas(windowWidth, windowHeight);
    // Quand une seule valeur est entrée, elle est utilisée pour le rouge, le vert et le bleu (donc toujours niveaux de gris)
    background(240);
}
function draw() {
    // On trace une ligne de la position précédente de la souris jusqu'à sa position actuelle
    line(pmouseX, pmouseY, mouseX, mouseY);
}
function keyPressed() {
    // On exécute le code ci-dessous quand une touche est pressée
    save("mondessin.jpg");
    // On envoie une information dans la console
    console.log("Dessin sauvé!");
}
```

## Quelques projets :

+ Création d'interfaces: [http://bit101.github.io/quicksettings/](http://bit101.github.io/quicksettings/) ou [https://github.com/bitcraftlab/p5.gui](https://github.com/bitcraftlab/p5.gui)
+ from net2theart: [http://natura-morta.com/index.html](http://natura-morta.com/index.html)
+ [TextFreeBrowsing](https://chrome.google.com/webstore/detail/text-free-browsing/ioglfbphilinnhdmfbmfljmhemegfcdg/) is a **Chrome extension** by Jonas Lund & Rafael Rozendaal : http://textfreebrowsing.com/
+ [https://hydra-editor.glitch.me/](+ https://hydra-editor.glitch.me/)
+ [Der Spiegel (p5js)](https://alsino.io/spiegel-time-compression/?fbclid=IwAR0V-8_ZWNsnnohmEewjo0yzJe44D_a9tqzmXLQooryVxAOBIR6a1O59qJw)
+ **Manipuler le DOM** : https://editor.p5js.org/jps723/sketches/Bk_E-k5hZ OU https://editor.p5js.org/jps723/sketches/HJyPPJc3W
+ **Jonathan Puckey** ([Pointer pointer](https://pointerpointer.com/) ou [Radio garden](https://radio.garden/listen/radio-emotion/dHIE7Ji1), )
+ [**Block Bills**](https://lumenprize.com/artwork/block-bills/) par Matthias Dorfelt. (aussi [ici](https://www.mokafolio.de/works/BlockBills))
+ Interfaces collection : [http://lab.gildasp.fr/guic/](http://lab.gildasp.fr/guic/)
+ US debt clock : [http://www.usdebtclock.org/#](http://www.usdebtclock.org/#)
+ Alexei Shulgin : [http://archive.rhizome.org/anthology/form-art.html](http://archive.rhizome.org/anthology/form-art.html)
+ [http://recodeproject.com/](http://recodeproject.com/) a disparu...
+ Sonic Pi : [https://sonic-pi.net/](https://sonic-pi.net/) ( [https://www.youtube.com/watch?v=UrfqA7ShYE0](https://www.youtube.com/watch?v=UrfqA7ShYE0) ou [https://www.youtube.com/watch?v=cydH_JAgSfg](https://www.youtube.com/watch?v=cydH_JAgSfg) )
+ Typography : [https://www.schultzschultz.com/G.mp4](https://www.schultzschultz.com/G.mp4)
