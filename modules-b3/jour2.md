---
title: Les données
lang: fr-FR
---

## Datas

<div class="lead">
<p>"Le réel est rythmique, tout à la fois vivant et chiffrable, sans qu'il y ait là une contradiction."</p>
<p style="font-size:13px;">Anne Teresa De Keersmaeker au <a href="https://www.rosas.be/fr/news/733-video-conference-d-anne-teresa-de-keersmaeker-au-college-de-france" target="_blank">Collège de France</a> (8'20''), à propos de cette <em title="Il y a des imbéciles qui définissent mon œuvre comme abstraite, pourtant ce qu’ils qualifient d’abstrait est ce qu’il y a de plus réaliste, ce qui est réel n’est pas l’apparence mais l’idée, l’essence des choses">fameuse citation de Brancusi</em>.</p>
</div>

<div style="padding:62% 0 0 0;position:relative;margin:2rem 0;border-radius:5px;"><iframe src="https://player.vimeo.com/video/154839056?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:5px;" frameborder="0" autoplay allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

> Exhausting a crowd, de [Kyle McDonald](https://kylemcdonald.net/)

+ Quelques exemples
+ Matin: infos + recherche. PM: présentation + recherche et workshop. Présentation en fin de deuxième journée
+ https://zkm.de/en/exhibition/2017/10/open-codes
+ Formats json: Une [page très complète](https://la-cascade.io/json-pour-les-debutants/) sur le format.
+ Les objects
+ [https://www.mediawiki.org/wiki/API:Main_page](https://www.mediawiki.org/wiki/API:Main_page)
+ Data driven documents : https://d3js.org/
+ https://www.zooniverse.org/projects/zooniverse/floating-forests
+ https://www.quora.com/Where-can-I-find-public-or-free-real-time-or-streaming-data-sources
+ https://www.octoparse.com/blog/big-data-70-amazing-free-data-sources-you-should-know-for-2017?qu
+ A simple node package to convert iCal data (.ics file) to JSON format : https://github.com/adrianlee44/ical2json
+ Does the data exists : https://opendatabarometer.org/4thedition/detail-country/?_year=2016&indicator=ODB&detail=BEL
+ https://www.courses.tegabrain.com/cc18/json-basics/
+ https://developer.wordnik.com/docs#!/
+ https://github.com/dariusk/corpora/blob/master/data/geography/countries.json
+ https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&limit=10&orderby=time
+ https://earthquake.usgs.gov/fdsnws/event/1/
+ http://api.open-notify.org/astros.json
+ Giphy

## Websockets

## Datasets & APIs

+ https://github.com/NVlabs/ffhq-dataset
+ https://next.json-generator.com/api/json/get/4JCnNiTCr
+ APIs publiques: https://github.com/public-apis/public-apis
+ https://github.com/Einenlum/french-verbs-list
+ https://opendata.stib-mivb.be/store/data
+ https://www.infotec.be/fr-be/minformer/opendata.aspx
+ Formats json
+ Data driven documents : https://d3js.org/
+ https://www.zooniverse.org/projects/zooniverse/floating-forests
+ https://www.quora.com/Where-can-I-find-public-or-free-real-time-or-streaming-data-sources
+ https://www.octoparse.com/blog/big-data-70-amazing-free-data-sources-you-should-know-for-2017?qu
+ A simple node package to convert iCal data (.ics file) to JSON format : https://github.com/adrianlee44/ical2json
+ Does the data exists : https://opendatabarometer.org/4thedition/detail-country/?_year=2016&indicator=ODB&detail=BEL

<a data-fancybox title="Synchronous Objects" href="/assets/synchronous1.jpg">![William Forsythe's Synchronous Objects for One Flat Thing](/assets/synchronous1.jpg)</a>

<a data-fancybox title="Synchronous Objects" href="/assets/synchronous2.jpg">![William Forsythe's Synchronous Objects for One Flat Thing](/assets/synchronous2.jpg)</a>

[William Forsythe's Synchronous Objects for One Flat Thing](https://synchronousobjects.osu.edu/)

## Pixels array


```processing
PImage img;
float posx, posy, coteH, coteV, nbrH, nbrV;
color maCouleur;

void setup() {
  size(800,800);
  background(0);
  noStroke();
  posx = 0;
  posy = 0;
  nbrH = 800;
  nbrV = 800;
  coteH = width/nbrH;
  coteV  = height/nbrV;
  img = loadImage("earth800.jpg");
  img.loadPixels();
  //translate(0,30);
  for(int i=0; i<img.pixels.length;i++) {
    float r = red(img.pixels[i]);
    float v = green(img.pixels[i]);
    float b = blue(img.pixels[i]);
    float a = alpha(img.pixels[i]);
    /*if(r < 50 && v < 100 && b < 150) {
      
    } else {
      
    }*/
    fill(r,v,b,a);
      rect(posx, posy, coteH, coteV);
    if(posx >= width) {
      posx = 0;
      posy += coteV;
    } else if (posy > height) {
      noLoop();
    }
    posx += coteH;
  }
  img.updatePixels();
}
```

+ US debt clock : http://www.usdebtclock.org/#

[Source](https://synchronousobjects.osu.edu/media/inside.php?p=gallery)

```processing
int x = 0;
int largeur, hauteur;
float teinte, saturation, luminosite;

void setup() {
  size(1230,1230);
  background(220);
  rectMode(CENTER);
  colorMode(HSB, 360, 100, 100);
  noStroke();
  largeur = 1230;
  hauteur = 1230;
}
void draw() {
  teinte = random(0,360);
  saturation = random(20,50);
  luminosite = random(20,70);
  fill(teinte, saturation, luminosite);
  rect(width/2, height/2, largeur, hauteur);
  largeur -= 5;
  hauteur -= 5;
  if(hauteur < 20 || largeur < 40) {
    noLoop();
  }
}
```
Avec boucle for dans le setup
```processing
float teinte, saturation, luminosite;

void setup() {
  size(1230,1230);
  background(220);
  rectMode(CENTER);
  colorMode(HSB, 360, 100, 100);
  noStroke();
  for(int i=1230;i>40;i-=5) {
    teinte = random(0,360);
    saturation = random(20,50);
    luminosite = random(20,70);
    fill(teinte, saturation, luminosite);
    rect(width/2, height/2, i, i);
  }
}
```

-------------

## xr6 1

Mise en place de projet

## xr6 2 

Balise canvas: Dessin dans la page web

## xr6 3 

noCanvas (ou pas): Création d'éléments html + jQuery

## xr6 4
