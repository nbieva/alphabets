---
title: interfaces
lang: fr-FR
---
<a class="backhome" href="/modules-b3/j1.html"><i class="material-icons">keyboard_arrow_left</i>Retour</a>

# Interfaces

<p class="lead">Avant de plonger dans la création de notre projet à proprement parler, il nous faut mettre en place les différents éléments qui le compose et un environnement de développement sain et fluide, histoire de ne pas nous préoccuper de soucis techniques inutiles par la suite..</p>

## Editeurs

+ Créer son application: [Création d'une interface](https://github.com/bitcraftlab/p5.gui) + [https://aatishb.com/p5-vue-starter/data-watching/](https://aatishb.com/p5-vue-starter/data-watching/) + [https://kinrany.github.io/vue-p5-example/](https://kinrany.github.io/vue-p5-example/)
+ [https://p5js.org/examples/dom-input-and-button.html](https://p5js.org/examples/dom-input-and-button.html)
## Exemples

+ http://mfviz.com/flowFields/
+ https://msurguy.github.io/flow-lines/
+ [https://p5studio.timrodenbroeker.now.sh/](https://p5studio.timrodenbroeker.now.sh/)