---
title: Images et pixels
lang: fr-FR
---

# Images et pixels

Cette page reprend à la fois les méthodes pour afficher les images et celles permettant d'accéder au pixels mêmes d'une image et de les modifier en fonction de telle ou telle condition.

> Une image numérique n'est rien de plus que des données - des nombres indiquant des variations de rouge, vert et bleu à un endroit particulier sur une grille de pixels. La plupart du temps, nous considérons ces pixels comme des rectangles miniatures pris en sandwich sur un écran d'ordinateur. Cependant, avec un peu de réflexion créative et une manipulation de niveau inférieur des pixels avec du code, nous pouvons afficher ces informations d'une multitude de façons.

[Dan Shiffman](https://www.processing.org/tutorials/pixels/) sur le site de Processing.

<a style="display:block;margin:2.5rem 0 0;" data-fancybox title="Buddha" href="/assets/annialbers.png">![Pixels](/assets/annialbers.png)</a>

Source de l'image: [https://medium.com/inside-vbat/anni-albers-fd3254325c92](https://medium.com/inside-vbat/anni-albers-fd3254325c92)

## Charger et afficher une image dans votre sketch

Une image se charge dans votre sketch en 3 étapes: 

1. **Initialiser** une variable de type **PImage** (Crée une instance de la classe PImage)
2. **Charger** l'image dans cette variable à l'aide de la fonction **loadImage()**
3. **Utiliser** l'image ou l'afficher simplement

```processing
PImage img; // Déclaration de la variable image

void setup() {
    size(1000, 520);    
    img = loadImage("anni.png"); // Chargement de l'image dans la variable
}

void draw() {
    image(img, 0, 0); // Affichage de l'image
}
```
::: tip
**Note:** Processing accepte les formats de fichiers GIF, JPG, TGA, PNG pour les images.
:::

Une fois chargée, l'image peut être utilisée comme outil de dessin, comme on pouvait le faire avec une ellipse ou une ligne.

```processing
PImage img;

void setup() {
    size(1000, 520);    
    img = loadImage("anni.png");
}

void draw() {
    if(mousePressed) {
        image(img, mouseX, mouseY);
    }
}
```

## La fonction image()

La fonction **image()** prend par défaut 3 paramètres. Le premier est **l'image elle-même** (dans la plupart des cas le nom de la variable). Les deuxième et troisième paramètres correspondent à la position en **x** et en **y** de l'image dans l'espace de dessin.

L'ajout de deux paramètres supplémentaires permet de définir une **largeur** et une **hauteur** à l'image dans notre sketch.

Pour travailler de façon plus flexible, nous pouvons utiliser les propriétés **width** et **height** de l'élément image.

```processing
PImage img;

void setup() {
    size(1000, 520);    
    img = loadImage("anni.png");
}

void draw() {
    if(mousePressed) {
        image(img, mouseX, mouseY, img.width/3, img.height/3);
    }
}
```

<a style="display:block;margin:2.5rem 0 0;" data-fancybox title="Anni" href="/assets/anni2.png">![Pixels](/assets/anni2.png)</a>

# Pixels!

## Pixels sans 'image'

De temps en temps, il peut être utile pour nous de quitter nos ellipses, rectangles et autres lignes pour nous aventurer un niveau plus bas, au niveau des pixels mêmes de notre espace de travail.
Ce sont eux que nous allons manipuler directement (ce que Processing fait en général pour nous, par exemple lorsque nous dessinons une ellipse).

l'exemple ci-dessous redéfini chaque pixel de notre format en lui assignant une valeur de gris aléatoire, et ce chaque fois que la souris est pressée.

```processing
void setup() {
  size(740, 400);
  loadPixels();
}
void draw() {
  if(mousePressed) {
    for(int i=0; i<pixels.length; i++) {
      float rand = random(255);
      color c = color(rand);
      pixels[i] = c;
    }
  }
  updatePixels();
}
```

<a style="display:block;margin:2.5rem 0 0;" data-fancybox title="Anni" href="/assets/bruit.png">![Pixels](/assets/bruit.png)</a>

En général, nous considérons les images comme des grilles de pixels avec une largeur et une hauteur. C'est effectivement ce qui s'affiche sur nos écrans. 
Cependant la réalité des pixels est légèrement différente. Ils sont en fait stockés sur un seul niveau, comme une liste à une seule dimension donc. Il s'agit d'un tableau (Array) qui peut ressembler à ceci:

<a style="display:block;margin:2.5rem 0 0;" data-fancybox title="Anni" href="/assets/pixels.png">![Pixels](/assets/pixels.png)</a>

Nous pouvons rendre disponible ce tableau dans notre programme à l'aide de la fonction **loadPixels()**.

Nous pouvons ensuite "remplir" notre sketch pixel par pixel à l'aide des **boucles for()** et en créant notre propre logique.

Pour mettre à jour l'affichage du tableau de pixels, il nous faut également utiliser la fonction **updatePixels()**.

```processing
void setup() {
  size(1000, 520);
  loadPixels();
}
void draw() {
  if(mousePressed) {
    for(int i=0; i<pixels.length; i++) {
      float rand = random(255);
      color c = color(rand);
      pixels[i] = c;
    }
  }
  updatePixels();
}
```

## Pixels d'une image

La fonction **loadPixels()** de Processing charge, par défaut, l'ensemble des pixels de l'espace de dessin et les place dans un tableau. Nous pouvons par la suite accéder aux valeurs de rouge, de vert et de bleu de chaque pixel, ainsi qu'à leur teinte, luminosité ou saturation, en utilisant l'index (position) de chaque pixel dans le tableau. 

Si votre image a été dessinée auparavant, vous pourrez donc accéder aux données des pixels de cette même image de cette manière. Mais il vous sera plus simple de vous y prendre de cette manière:

```processing
img.pixels[45] // Pour récupérer la couleur du 46e pixel du tableau, par exemple.
```

De plus, une fois qu'une image est chargée (ligne 5), vous pouvez utiliser ses propriétés **width** et **height** pour récupérer largeur et hauteur dynamiquement (voir ligne 9)

```processing
PImage img;

void setup() {
  size(1000, 520);    
  img = loadImage("anni.png");
}

void draw() {
  image(img, mouseX, mouseY, img.width/5, img.height/5);
}
```
<a style="display:block;margin:2.5rem 0 0;" data-fancybox title="Anni" href="/assets/anni3.png">![Pixels](/assets/anni3.png)</a>

```processing
PImage img;
int pointillize = 10;

void setup() {
  size(1000,520);
  img = loadImage("anni.png");
  background(0);
  smooth();
  rectMode(CENTER);
}

void draw() {
  // On définit un point aléatoirement
  int x = int(random(img.width));
  int y = int(random(img.height));
  int loc = x + y*img.width;
  
  // On va chercher les réferences R, V et B du pixel en question dans l'image
  loadPixels();
  float r = red(img.pixels[loc]);
  float g = green(img.pixels[loc]);
  float b = blue(img.pixels[loc]);
  
  noStroke();// Pas de contour
  fill(r,g,b,100);// Couleur avec opacité réduite (100/255)
  rect(x,y,pointillize*3,pointillize);
}
```

<a style="display:block;margin:2.5rem 0 0;" data-fancybox title="Pixels" href="/assets/pixels.jpg">![Pixels](/assets/pixels.jpg)</a>
> Les formules respectives, dans Processing et P5js, pour accéder à l'emplacement d'un pixel particulier dans le tableau des pixels du canvas ou d'une image.

## Legofactor

<a data-fancybox title="Lego" href="/assets/lego.png">![lego](/assets/lego.png)</a>

Ci-dessus le post de Margot à propos du travail de Geoffroy Amelot, qui nous inspire cette petite adaptation via Processing. (Comment il s'y prend dans Photoshop pour faire ceci reste un mystère..) 

Nous utilisons ici le mode colorimétrique **HSB** qui nous permet plus facilement d'agir sur la **luminosité** d'une couleur ou sa **saturation** sans toucher à sa **teinte**, ce qui serait beaucoup plus complexe en RVB.

**Le mode HSB est déclaré en début de programme** (ligne 10). Les trois derniers paramètres indiquent à processing la fourchette de valeur avec laquelle nous désirons coder nos couleurs (ici entre 0 et 100 pour les trois composantes). Souvent, la composante teinte utilise une fourchette de 360 ([exemple ici](http://localhost:8080/processing/couleur.html#mode-hsb)), corrrespondant aux degrés du cercle chromatique. 
Mais le choix est libre...

<a style="display:block;margin:2.5rem 0 0;" data-fancybox title="Anni" href="/assets/anni4.png">![Pixels](/assets/anni4.png)</a>

```processing
PImage img;
int nbr = 40;
int cote;
float ombre, dia;

void setup() {
  size(1000,520);
  img = loadImage("anni.png");
  noStroke();
  colorMode(HSB,100,100,100);
  rectMode(CENTER);
  ellipseMode(CENTER);
  cote = int(width/nbr);
  dia = cote-cote/3;
  ombre = cote/25;
  for(int y=cote/2;y<height;y+=cote) {
    for(int x=cote/2;x<width;x+=cote) {
      int pos = x+(y*width);
      float h = hue(img.pixels[pos]);
      float s = saturation(img.pixels[pos]);
      float b = brightness(img.pixels[pos]);
      fill(h,s,b-7);
      rect(x,y,cote,cote);
      fill(h,s,b-25);
      ellipse(x-ombre,y-ombre,dia, dia);
      fill(h,s,b+10);
      ellipse(x+ombre,y+ombre,dia, dia);
      fill(h,s,b);
      ellipse(x,y,dia, dia);
    }
  }
}

void draw() {}

void keyPressed() {
  save("fargues.jpg");
}
```

<a style="display:block;margin:2.5rem 0 0;" data-fancybox title="Anni" href="/assets/anni5.png">![Pixels](/assets/anni5.png)</a>