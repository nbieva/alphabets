---
title: Web & P5js
lang: fr-FR
---

# P5*js et le web

+ Quelques exemples

### Objectifs de la journée

+ redraw()
+ http://workshop.chromeexperiments.com/
+ Passer de Processing à P5js
+ HTML, CSS, SASS
+ Mise en place d'un projet web: rappel (netlify, domaines, etc. ) + Architecture
+ Editeurs de code + Live server
+ Code et web: jQuery, Nodejs, Angular, Vue, etc.
+ Mais aussi python et Django (voir arthur?)
+ Javascript dans la page web (basic bootstrap + [jQuery](https://jquery.com/) )
+ L'élément HTML Canvas
+ P5js + Télécharger P5js ou CDN + p5-manager
+ Quand utiliser HTML attributes/CSS/JS/Processing/etc.
+ Passer de Processing à P5js
+ Dessiner dans le Canvas
+ P5js editor + Création de compte sur P5js
+ Manipulation du DOM (éléments HTML - création, suppression, modification)
+ Créer une interface simple dans la page web
+ Travailler avec des gifs animés
+ Mise en ligne avec Netlify

### Fonctions spécifiques

+ createGraphics (Extra canvas) https://www.youtube.com/watch?v=TaluaAD9MKA&index=13&t=0s&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA
+ Drag and drop. Déposer un fichier dans une page et l'utiliser

<a data-fancybox title="p5js" href="/assets/p5schema.png">![Boucles](/assets/p5schema.png)</a>

+ Exemples P5js (Lauren McCarthy): [https://github.com/lmccart/gswp5.js-code](https://github.com/lmccart/gswp5.js-code)
+ [Alan Kay, "The Dynabook—Past Present and Future"](https://www.youtube.com/watch?v=GMDphyKrAE8)
+ https://www.youtube.com/watch?v=r36NNGzNvjo
+ https://github.com/jpf/wikigifs


## Concretement

+ Créer un projet web à partir d'un sketch Processing et le mettre en ligne sur Netlify
+ Création d'une grille de GIFs animés en utilisant les boucles for + le pseudo attribut CSS :hover pour modifier leur taille au survol.
+ Nom de domaine personnalisé
+ Créer un projet P5js
+ Savoir organiser ses assets
+ Ajouter des bibliothèques
+ Mettre un site en ligne sur Gitlab avec auto-deploiment sur Netlify
+ Créer une extension Chrome