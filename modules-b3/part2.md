---
title: Notions fondamentales
lang: fr-FR
---

<p class="lead">On poursuit avec les grands principes du code, en introduisant les conditions et les boucles. En fonction du temps, nous aborderons également le travail de l'image et la manipulation d'éléments HTML.</p>

<a data-fancybox title="Micro:bit" href="/assets/microbit.png">![Micro:bit](/assets/microbit.png)</a>
> L'interface de programmation par blocs de micro:bit, avec l'équivalent des fonctions setup() et draw() de Processing ou P5js.

# Part 2: Dessin 2

## Il y a une semaine:

+ Introduction générale au workshop et au code
+ Processing et P5js, quid?
+ Les bibliothèques
+ Création d'un compte sur l'éditeur de P5js : https://editor.p5js.org/
+ Articulation HTML/CSS/JS et architecture d'un projet
+ L'élément [canvas](https://www.w3schools.com/html/html5_canvas.asp)
+ Création d'un sketch P5
+ Système de  coordonnées, couleur et modes colorimétriques, formes (rect, ellipse, line, etc..)
+ Structure d'un programme et boucles
+ Les variables natives + Création de variables
+ Editeurs de code: astuces
+ Lancer un serveur local dans l'éditeur (on a utilisé [VS Code](https://code.visualstudio.com/) et [Live server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer))
+ Liens entre fichier HTML, feuille de style CSS, P5js, et autres scripts.
+ Test

## Fonctions évoquées la semaine dernière:

+ setup(), draw()
+ size(), windowWidth(), windowHeight()
+ rect(), ellipse(), line(), etc... (cfr reference)
+ rectMode()
+ fill(), stroke(), strokeWeight(), noStroke(), noFill()

## Au programme aujourd'hui

+ **Créer > Déployer > Partager** (netlify)
+ L'**aléatoire**
+ Les **conditions**
+ Les **opérateurs logiques**
+ hour(), minute(), second(), millis()
+ L'**interaction** (Souris, clavier, micro...)
+ Les **boucles**
+ **Création d'une horloge**
+ Atelier
+ Résumé