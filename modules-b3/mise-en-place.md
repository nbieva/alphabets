---
title: Mise en place d'un projet web
lang: fr-FR
---
<a class="backhome" href="/modules-b3/j1.html"><i class="material-icons">keyboard_arrow_left</i>Retour</a>

# Mise en place d'un projet web

<p class="lead">Avant de plonger dans la création de notre projet à proprement parler, il nous faut mettre en place les différents éléments qui le compose et un environnement de développement sain et fluide, histoire de ne pas nous préoccuper de soucis techniques inutiles par la suite..</p>

## Editeurs

+ Les différents éditeurs disponibles : [VScode](https://code.visualstudio.com/), [Sublime text](https://www.sublimetext.com/), [Atom](https://atom.io/), [Brackets](http://brackets.io/), ...
+ Settings (Cmd + ?)
+ [Themes](https://vscodethemes.com/) (Cmd + K + T)
+ [Rechercher/remplacer + curseurs multiples](https://docs.microsoft.com/en-us/visualstudio/ide/finding-and-replacing-text?view=vs-2019)
+ Multifenêtre
+ Les extensions importantes ([Live server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer), live reload, Sass..)
+ Le [terminal](https://code.visualstudio.com/docs/editor/integrated-terminal) dans l'interface (+ multiples fenêtres terminal)
+ Ligne de commande: [Basics](https://www.courses.tegabrain.com/cc18/github-and-command-line-basics/)
+ [Gestion de version](https://code.visualstudio.com/docs/editor/versioncontrol) dans l'interface. Terminal, [iTerm](https://iterm2.com/) and [Git Bash](https://www.atlassian.com/git/tutorials/git-bash)
+ Les éditeurs en ligne: [https://codesandbox.io/](https://codesandbox.io/), [Codepen](https://codepen.io/), [jsfiddle](https://jsfiddle.net/), l'[éditeur de P5](https://editor.p5js.org/) etc.
+ NodeJS
+ L'architecture d'un projet
+ Faire tourner un serveur local (VScode ou [Python](https://www.courses.tegabrain.com/cc18/github-and-command-line-basics/))
+ [HTML](https://jenseign.com/), [CSS](https://fr.wikipedia.org/wiki/Feuilles_de_style_en_cascade), [SASS](https://sass-lang.com/)
+ Déploiement d'une simple page html avec un serveur local

<a data-fancybox title="vscode" href="/assets/vscode-screen.png" style="margin:2rem 0;display:block;">![vscode](/assets/vscode-screen.png)</a>

## Le navigateur

+ Inspecteur et console
+ Différents navigateurs
+ Styles dans Firefox
+ Sauver une feuille de style temporaire

## La documentation

+ [Markdown](/les-indispensables/documentation.html) + MacDown
+ Git Github

```markdown
## Liste à puces

+ Premier [lien](http://lacambre.be)
+ Deuxième [lien](http://example.com)
```

## Pré-processeurs CSS

+ SASS et LESS
+ Principe des pré-processeurs et exemple sur la couleur d'un bouton.
+ Stylus: [http://stylus-lang.com/](http://stylus-lang.com/)
+ Emmet: [https://emmet.io/](https://emmet.io/)
+ Pré-processeurs CSS
+ Liste des règles CSS et éléments HTML: [https://developer.mozilla.org/fr/docs/Web/HTML/Element](https://developer.mozilla.org/fr/docs/Web/HTML/Element)
+ Post-processing

![](https://raw.githubusercontent.com/ritwickdey/vscode-live-sass-compiler/master/images/Screenshot/AnimatedPreview.gif)

## Préparer nos fichiers pour le partage

+ Paramétrer pour le partage: [https://developers.facebook.com/tools/debug](https://developers.facebook.com/tools/debug)
+ Pour twitter, par exemple ici: [https://otherside.otherti.me/](https://otherside.otherti.me/)

## Tests

+ tests

## Déploiement

+ Local/Dev/Prod
+ Git Github
+ Ligne de commande
+ Source tree
+ [Netlify](https://www.netlify.com/), domaines, etc. 
+ Custom domain
+ Autre possibilités (Digital Ocean etc.) On y reviendra plus tard.
+ Mise en ligne + Nom de domaine personnalisé
+ Mise en ligne sur [Gitlab](https://gitlab.com/) ou [Bitbucket](https://bitbucket.org/) avec auto-deploiment sur Netlify