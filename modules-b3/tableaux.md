---
title: Les tableaux
lang: fr-FR
---

# Les tableaux

Un tableau (ou liste) est une **suite de variables**. Vous croiserez souvent sa syntaxe lors de vos recherches. Chaque tableau ne peut contenir que des variables **du même type**.
Chaque item du tableau est **accessible via son index** (0, 1, 2, 3, ..), entouré de crochets []. Nous verrons, avec javascript, que tout peut être variable (fonctions, nombres, chaînes de caractères, objets, ... )

<a data-fancybox title="Pixels" href="/assets/array.png">![Pixels](/assets/array.png)</a>

```processing
int[] values = {8, 9, 10, 11};

void setup() {
  size(800, 600);
  text(values[2],50,50);
}
```

```processing
int[] values = {5,-3,78,5,-5896,4};

void setup() {
  size(800, 600);
  for(int i=0;i<values.length;i++) {
      prinln(values[i]);
  }
}
```

```processing
int[] values;

void setup() {
  size(800, 600);
  values = new int[8];
  for(int i=0;i<values.length;i++) {
      prinln(values[i]);
  }
}
```

### Tableaux de chaînes de caractères

```processing
String[] sujets = {"Vert","Rouge","Jaune","Bleu"};
float posx, posy;

void setup() {
  size(800, 600);
  background(255);
  noStroke();
  frameRate(2);
  textSize(18);
}

void draw() {
  fill(255,80);
  rect(0,0,width,height);
  fill(0);
  for(int i=0;i<sujets.length;i++) {
    posx = random(width);
    posy = random(height);
    text(sujets[i], posx, posy);
  }
}
```

::: warning
Réalisez un cadavre exquis en utilisant 3 tableaux différents (sujets, verbes, compléments) de 10 entrées chacun. Une phrase différente doit apparaître chaque seconde.
:::

+ https://www.processing.org/tutorials/arrays/
+ https://www.processing.org/tutorials/2darray/

### Enregistrer des données

```processing
// Définittion du nombre de points à enregistrer (à stocker dans les tableaux)
int num = 75;
int[] x = new int[num];
int[] y = new int[num];

void setup() { 
  size(800, 800);
  noStroke();
  fill(255, 102);
}

void draw() {
  background(0);
  // Décale les valeurs du tableau vers la droite
  for (int i = num-1; i > 0; i--) {
    x[i] = x[i-1];
    y[i] = y[i-1];
  }
  // Ajoute les nouvelles valeurs au début des tableaux
  x[0] = mouseX;
  y[0] = mouseY;
  
  // Dessine les cercles en parcourant les tableaux
  for (int i = 0; i < num; i++) {
    ellipse(x[i], y[i], i, i);
  }
}
```

---------

+ Les **fonctions personnalisées**
+ P5js
+ [HTML, CSS](https://webb2.netlify.com/) etc
+ L'[élément Canvas](https://www.w3schools.com/html/html5_canvas.asp)
  
## Les images

### Charger et afficher une image

```processing
PImage img; // déclaration de la variable image

void setup() {
    size(600, 600);    
    img = loadImage("workshop.jpg");
}

void draw() {
    image(img, 0, 0, width/2, height/2);
}
```

+ preload()


## Et en vrac

+ colorMode **HSB**
+ **loop()**, **noLoop()**
+ Les **tableaux** (listes, ou arrays) + Exercice (voir ci-dessous)
+ Concaténation
+ Charger les pixels d'une image et les manipuler.
+ Le [texte dans Processing](/en-pratique/texte)
+ Export dans fichier texte
+ Savoir exporter en PDF/JPG/SVG
+ Les [bibliothèques](/generalites/outils.html#les-bibliotheques)
+ Créez un cadavre exquis à partir de différents tableaux (sujets, verbes, compléments). Une phrase doit apparaître à chaque clic de souris, à l'enroit du clic. Le rendu visuel est laissé à votre libre appréciation. + export ds fichier
+ Autre chouette projet pour plus tard: recréer sa tl fb ou twitter ou une tl fictive.
+ Imports d'images, de formes (SVG), de sons, video etc. (> Bibliothèques)
+ Pour nos travaux (en ligne) : [Google sheet](https://docs.google.com/spreadsheets/d/1dxu-R_-tVmMC1_ASeh2irF8KN_TcgBhj4uTuAWUftnw/edit?usp=sharing)
+ **sin()** et **cos()**
+ **lerpColor()**
+ **dist()**, **map()**



# Arrays: Declare, create, assign

https://chevalvert.fr/en/projects/murmur

+ Exemples P5js (Lauren McCarthy): [https://github.com/lmccart/gswp5.js-code](https://github.com/lmccart/gswp5.js-code)

+ [Alan Kay, "The Dynabook—Past Present and Future"](https://www.youtube.com/watch?v=GMDphyKrAE8)
+ https://www.youtube.com/watch?v=r36NNGzNvjo

<a data-fancybox title="P5js" href="/assets/generative.png">![P5js](/assets/generative.png)</a>

[http://www.generative-gestaltung.de/2/](http://www.generative-gestaltung.de/2/)