---
title: Visualiser le temps
lang: fr-FR
---

+ Moment.js
+ [Babylonian hours](https://www.babylonianhours.com/) + Docs
+ Other time: [https://otherti.me/](https://otherti.me/)

## CANVAS 1

+ canvas full screen
+ background:rouge
+ A chaque clic: on dessine une ellipse de taille entre 30 et 150px
+ Couleur de l'éllipse différente à chaque fois, mais plutôt jaune
+ Sous chaque ellipse: l'heure a	u format HH:MM:SS
+ Si on est entre la seconde 30 et la seconde 60, l'ellipse a un countour blanc de 10px
+ Quand on appuye sur une touche, l'image est exportée en JPG et porte le nom capture-HH-MM-SS.jpg

## CLOCK

+ [http://golancourses.net/2015/lectures/visualizing-time/](http://golancourses.net/2015/lectures/visualizing-time/)
+ https://www.courses.tegabrain.com/cc18/visualizing-time/
+ [History of timekeeping devices](https://en.wikipedia.org/wiki/History_of_timekeeping_devices)
+ http://cmuems.com/2016/60212/lectures/lecture-09-09b-clocks/
+ http://cmuems.com/2016/60212/resources/drucker_timekeeping.pdf
+ [An entire history of time measurement in six minutes.](https://www.youtube.com/watch?v=SsULOvIWSUo) (frome [here](http://cmuems.com/2016/60212/deliverables/deliverables-02/)) !!!
+ [https://www.humanclock.com/](https://www.humanclock.com/)
+ Moment and P5js: [https://editor.p5js.org/kjhollen/sketches/B1t0ov4ab](https://editor.p5js.org/kjhollen/sketches/B1t0ov4ab)
+ [https://editor.p5js.org/nicobiev/present/cWneuXsNd](https://editor.p5js.org/nicobiev/present/cWneuXsNd)
+ [https://editor.p5js.org/NicolasB3/present/BhPqaJbNt](https://editor.p5js.org/NicolasB3/present/BhPqaJbNt)

## Autres

+ Construire une interface pour générer des images
+ Création d'une image (ou image animée)
+ GifLab (animer des gifs animés, inspiré par evan roth)
+ En utilisant les **formes simples** et leurs paramètres (**couleur**, contour, etc..), les **transformations**, et en utilisant un maximum de **variables**, créez un générateur de visuels que vous devez ensuite pouvoir **exporter** en PNG ou PDF en appuyant sur une touche.