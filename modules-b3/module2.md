---
title: Conditions, boucles & interactivité
lang: fr-FR
---

## Conditions, boucles & interactivité

<video width="740" height="auto" controls autoplay style="border-radius:.5rem;">
  <source src="/assets/boucles.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
<a data-fancybox title="Boucles" href="/assets/boucles-grille.png" style="margin-bottom:2rem;display:block;">Image fixe</a>

+ Les [conditions](https://www.php.net/manual/fr/control-structures.if.php) ( + && et || )
+ Les **opérateurs** arithmétiques ( **<, >, <=, >=, ==, !=** )
+ **if, else if, else** + **variables [booléennes](https://fr.wikipedia.org/wiki/George_Boole)**
+ Exemples dans d'autres langages (PHP, Python, JavaScript..)
+ variables **pmouseX** et **pmouseY**
+ **day()**, **hour()**, **minute()**, **second()**, **millis()**
+ La **concaténation** (mise à la suite l'un de de l'autre de chaînes de caractères et de variables)
+ variables **mousePressed**, **keyPressed**
+ Interaction [souris](/en-pratique/interactivite-souris) et [clavier](/en-pratique/interactivite-clavier)
+ Les [boucles for](/processing/boucles) + **portée des variables** dans les boucles
+ Petite intro à [P5js](/generalites/processing-et-p5js.html#p5js) via l'[éditeur](https://editor.p5js.org/)

<a data-fancybox title="Boucles" href="/assets/boucles-principe.jpg">![Boucles](/assets/boucles-principe.jpg)</a>

### Syntaxe de base

```javascript
if (condition) {
    code to be executed if condition is true;
} else {
    code to be executed if condition is false;
}
```

### Processing

```processing
void setup() {
  size(800, 600);
}
void draw() {
  if(mousePressed) {
    line(pmouseX, pmouseY, mouseX, mouseY);
  }
}
```

### PHP

```php
<?php
$t = date("H");

if ($t < "20") {
    echo "Have a good day!";
} else {
    echo "Have a good night!";
}
?>
```

### Python

```python
a = 20
if a > 5:
  a = a + 1
else:
  a = a - 1 
a
```

## Boucles

+ Exemples dans d'autres langages

### while()
```processing
int compteur;

void setup() {
  size(800, 600);
  compteur = 10;
}
void draw() {
  while(compteur < height) {
    line(10, compteur, width-10, compteur);
    compteur+=10;
  }
}
```

### for()
```processing
void setup() {
  size(800, 600);
}
void draw() {
  for(int i = 10; i < height; i += 10) {
    line(10, i, width-10, i);
  }
}
```

### PHP

```php
<?php
for ($x = 0; $x <= 10; $x++) {
    echo "The number is: $x <br>";
}
?>
```

### Python

```python
for x in range(6):
  print(x)
```

or

```python
fruits = ["apple", "banana", "cherry"]
for x in fruits:
  print(x)
```

Créer un outil de dessin avec 3 commandes: couleur, clavier mouse, trait, fond.

## Interactivité

+ "The **keyPressed()** function is called once every time a key is pressed. The key that was pressed is stored in the key variable." (Reference Processing)
+ "The **mousePressed()** function is called once after every time a mouse button is pressed. The mouseButton variable (see the related reference entry) can be used to determine which button has been pressed." (Reference Processing)
"**Mouse and keyboard events only work when a program has draw()**. Without draw(), the code is only run once and then stops listening for events." (Reference Processing)


<a data-fancybox title="Pixels" href="/assets/angles.png">![Pixels](/assets/angles.png)</a>

# Exercices

Je vous propose de mettre en pratique les différents éléments abordés au cours (variables, conditions, boucles, fonctions..) en créant l'une des trois propositions suivantes (ou les trois bien entendu):

## 1. Horloge

Processing et P5js ont tous deux quelques variables natives liées au temps et permettant de récupérer en temps réel l'heure ( **hour()** ), la minute ( **minute()** ), la seconde ( **second()** ) et la milliseconde ( **millis()** ).

A partir de là, vous pouvez tout imaginer. Je vous propose donc de **créer une horloge** pour visualiser le temps qui passe d'une façon inédite. Cela peut-être créer un sketch purement visuel, ou manipuler les valeurs numériques (ce que renvoient ces fonctions) pour brouiller les pistes, travailler avec le texte (chaînes de caractères), couleurs, etc..

Songez toujours qu'une valeur (par exemple l'entier qui correspond à l'heure qu'il est) peut être utilisée pour une grande variété de choses (largeur d'une forme, teinte d'une couleur, opacité, contour, dimensions, etc..)

Vous pouvez réaliser ceci avec **Processing**, ou en profiter pour tester l'[éditeur en ligne de P5js](https://editor.p5js.org/) (C'est du JavaScript). Créez un compte, ils ne spamment pas! Cela vous permettra de sauver vos sketchs et de les partager en ligne.

[Cette page](https://codeb3.netlify.com/generalites/processing-et-p5js.html#passer-de-processing-a-p5js) reprend les quelques différences entre Processing et P5js pour passer de l'un à l'autre (ou vice-versa). Vous verrez que la syntaxe est fort semblable.

Vous pouvez le cas échéant utiliser [ce convertisseur en ligne](http://faculty.purchase.edu/joseph.mckay/p5jsconverter.html)

**Partagez** ensuite votre travail (le lien ou le code) sur notre page Facebook.

## 2. Trame

Avec Processing, et sur base de ce que nous avons vu au cours sur les boucles imbriquées, **créez une grille** avec une série de formes (simples [ou plus complexes](https://processing.org/reference/beginShape_.html)) qui se répètent et qui varient en fonction d'une **interaction souris et/ou clavier**.

Votre sketch doit faire plus ou moins 1200 de large sur 900 de haut. Par exemple..

[Une image doit être exportée](https://codeb3.netlify.com/en-pratique/exporter.html) au format JPG chaque fois que la touche 's' du clavier est pressée.

Vous pouvez tenter ensuite d'en faire une **version web avec P5js** (voir liens ci-dessus). Auquel cas, vous pouvez faire l'impasse sur l'export d'image.

**Partagez** ensuite votre travail (le lien ou le code) sur notre page Facebook.

## 3. Exercice graphique

Créez [ce visuel de Dan Walsh](https://www.google.com/search?q=dan+walsh&source=lnms&tbm=isch&sa=X&ved=2ahUKEwigzIv2m5DmAhUI1hoKHcIGCB4Q_AUoAXoECBAQAw&biw=1680&bih=899#imgrc=lbTdo0_yJbAjVM) en utilisant un minimum de code. Faites-en une version interactive ou animée.

Tentez d'en faire une **version web avec P5js** (voir liens ci-dessus). 

**Partagez!**