---
title: Les tableaux
lang: fr-FR
---


<p class="lead">Introduction aux tableaux (arrays) et à leur manipulation dans Processing ou P5js. Import et utilisation d'images dans un sketch, etc..</p>

# 3. Tableaux et images
<a data-fancybox title="Pixels" href="/assets/array.png">![Pixels](/assets/array.png)</a>

## Il y a une semaine
+ L'**aléatoire** avec la fonction random()
+ Les **conditions**
+ Les **boucles for**
+ **Interactivité**
+ La **concaténation**

## Aujourd'hui
+ Exercice récapitulatif (voir ci-dessous)
+ Les **tableaux** (listes, ou arrays) + Exercice (voir ci-dessous)
+ Tableaux et boucles
+ Les images : **Charger et utiliser une image** dans un sketch (+ preload).
+ **Sauver** une image


## Emma, ton code:

```javascript
function setup() {
    createCanvas(windowWidth,windowHeight);
    background(10,0,0);
    stroke(0,30);
}
function draw() {
    star(width/2, height/2, 40, mouseY, mouseX);
}

function star(x, y, radius1, radius2, npoints) {
    let angle = TWO_PI / npoints;
    let halfAngle = angle / 2.0;
    beginShape();
    for (let a = 0; a < TWO_PI; a += angle) {
      let sx = x + cos(a) * radius2;
      let sy = y + sin(a) * radius2;
      vertex(sx, sy);
      sx = x + cos(a + halfAngle) * radius1;
      sy = y + sin(a + halfAngle) * radius1;
      vertex(sx, sy);
    }
    endShape(CLOSE);
  }

function keyPressed() {
    if (hour()<12) {
        save("Capture d'étoile-"+hour()+"-"+minute()+"-"+second()+".jpg");
    }
}
```