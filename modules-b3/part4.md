---
title: Manipuler les pixels
lang: fr-FR
---

<p class="lead">Nous verrons également comment accéder à la liste des pixels d'une image et à leurs valeurs de rouge, vert, bleu et alpha, afin de créer nos propres filtres bitmap.</p>

#### Sandbox P5js:

+ [https://www.dropbox.com/s/2gynpnhd2m3iu72/sandbox-p5.zip?dl=0](https://www.dropbox.com/s/2gynpnhd2m3iu72/sandbox-p5.zip?dl=0)

# 4. Le web

<a data-fancybox title="Pixels" href="/assets/pixels.jpg">![Pixels](/assets/pixels.jpg)</a>
> Les formules respectives, dans Processing et P5js, pour accéder à l'emplacement d'un pixel particulier dans le tableau des pixels du canvas ou d'une image.

> [http://datamoshing.com/tag/processing/](http://datamoshing.com/tag/processing/)

+ Pourquoi faire simple...
+ Charger la **liste des pixels** d'une image donnée et la manipuler.
+ **Manipuler les données** d'un tableau (push, unshift, splice..)
+ Peut-être : **Pixel sorting** > trier les données d'un tableau.
+ GifLab : le cas particulier des Gifs animés.
+ **Fonctions personnalisées** (exemple star)
+ Code et web : HTML, CSS(SCSS), PHP, DB, jSon, JS, NodeJS, Vue, Angular, etc...
+ Les différentes façons d'interagir avec la page web avec javascript HTML CSS JS
+ Le cas P5js
* noCanvas
* Manipuler le DOM avec P5js
* Récupérer la valeur d'un input (text, date, colorpicker...)
* Ajout d'un bibliothèque comme P5 GUI ( https://github.com/bitcraftlab/p5.gui )
* Toggle Class ( + bootstrap exemple)

## Pistes
+ Faire une balle qui rebondit
+ Créer un cadavre exquis avec les tableaux
+ Dessiner avec une image. Importer un PNG. Sauvez l'image en appuyant sur un touche du clavier.
+ Créer une horloge
+ Créer un mode d'emploi dans le sketch.

```javascript
var monTexte = ["Hello", "Bonjour", "Holà", "Sayonara", "Hei!"];
var index;

function setup() {
  createCanvas(windowWidth, windowHeight);
  index = floor(random(monTexte.length));
  text(monTexte[index], 100,100);
  print(monTexte.length);
}
```

## [Drag and drop](https://editor.p5js.org/shiffman/sketches/rkwVCrIkl)

```javascript
var div;

function setup() { 
  createCanvas(200, 200);
  div = createDiv('drag a file here');
  div.style('padding', '20px');
  div.style('background-color', '#AAA');
  div.drop(fileDropped);
  div.dragOver(dragged);
  div.dragLeave(nodrag);
  
  //div.
} 

function dragged() {
  div.style( 'background-color', '#A0A');
}

function nodrag() {
  div.style( 'background-color', '#00A');
}


function fileDropped(file) {
  div.style( 'background-color', '#AAA');
  createImg(file.data);
  //image(file.data,0,0,width,height);
}

function draw() { 
  //background(220);
}
```

## Why not :
+ createGraphics (Extra canvas) https://www.youtube.com/watch?v=TaluaAD9MKA&index=13&t=0s&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA
+ Drag and drop. Déposer un fichier dans une page et l'utiliser.
+ [WebGL](https://www.youtube.com/watch?v=6TPVoB4uQCU)
+ Websockets (https://github.com/alphydan/piArt)
+ Raspberry Pi & Processing for PI
+ Gitbook et générateurs de sites statiques
+ NodeJS
+ Websockets
+ SASS
+ Vuejs
+ Chacun présente une fonction de P5.dom
+ http://paperjs.org/about/
+ https://threejs.org/