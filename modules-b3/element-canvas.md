---
title: L'élément canvas
lang: fr-FR
---
<a class="backhome" href="/modules-b3/j1.html"><i class="material-icons">keyboard_arrow_left</i>Retour</a>

# L'élément canvas

<p class="lead">L'élément HTML Canvas a fait son apparition avec HTML5, en même temps qu'un grand nombre d'autres éléments (main, header, footer, aside ...), dont certains en corrélation étroite avec Javascript (video, audio, drag and drop, canvas ...). Il permet, à l'aide de Javascript donc, de dessiner, créer ou manipuler des contenus graphiques directement dans la page web.</p>

<a data-fancybox title="canvas" href="https://artlogic-res.cloudinary.com/w_1000,h_1000,c_limit,f_auto,fl_lossy,q_auto/ws-galerieboulakia/usr/images/artists/artist_image/items/f2/f26a30579100497bb831c41fc7060dc3/hans_hartung_14.jpg" style="margin:2rem 0;display:block;">![canvas](https://artlogic-res.cloudinary.com/w_1000,h_1000,c_limit,f_auto,fl_lossy,q_auto/ws-galerieboulakia/usr/images/artists/artist_image/items/f2/f26a30579100497bb831c41fc7060dc3/hans_hartung_14.jpg)</a>

## P5js

+ p5-manager

## Le canvas

+ fullScreen()
+ https://discourse.processing.org/t/a-p5-js-cheat-sheet-for-beginners/8236
+ [Beyond the Canvas](https://github.com/processing/p5.js/wiki/Beyond-the-canvas): Dessiner dans la page web
+ Aléatoire : [https://www.courses.tegabrain.com/cc18/randomness-in-art/](https://www.courses.tegabrain.com/cc18/randomness-in-art/)
+ https://p5js.org/get-started/
+ 10PRINT: [https://editor.p5js.org/codingtrain/sketches/ryxWYgmwX](https://editor.p5js.org/codingtrain/sketches/ryxWYgmwX)

#### HTML5 Canvas

+ HTML [Canvas](https://www.w3schools.com/html/html5_canvas.asp)
+ Dessiner dans le Canvas (10 print)
+ P5js editor
+ Création de compte sur P5js

## 10 print

+ https://editor.p5js.org/codingtrain/sketches/ryxWYgmwX

```javascript
var monTexte = ["Hello", "Bonjour", "Holà", "Sayonara", "Hei!"];
var index;

function setup() {
  createCanvas(windowWidth, windowHeight);
  index = floor(random(monTexte.length));
  text(monTexte[index], 100,100);
  print(monTexte.length);
}
```

### Gradient

```javascript
function setup() {
  createCanvas(800, 600);
  //background(50);
}

function draw() {
  loadPixels();
  for(let x=0;x<width;x++) {
    for(let y =0;y<height;y++) {
      let pospix = (x+y*width)*4;
      pixels[pospix] = x/2;
      pixels[pospix+1] = y/2;
      pixels[pospix+2] = 100;
      pixels[pospix+3] = 255;
    }
  }
  updatePixels();
}
```