---
title: Arrays[] et pixels[]
lang: fr-FR
---

# Arrays[] et pixels[]

<a data-fancybox title="Pixels" href="/assets/pixels.jpg">![Pixels](/assets/pixels.jpg)</a>
> Les formules respectives, dans Processing et P5js, pour accéder à l'emplacement d'un pixel particulier dans le tableau des pixels du canvas ou d'une image.

<a data-fancybox title="Pixels" href="/assets/array.png">![Pixels](/assets/array.png)</a>



+ colorMode **HSB**
+ **loop()**, **noLoop()**
+ Les **tableaux** (listes, ou arrays) + Exercice (voir ci-dessous)
+ Concaténation
+ Charger les pixels d'une image et les manipuler.
+ Le [texte dans Processing](/en-pratique/texte)
+ Export dans fichier texte
+ Savoir exporter en PDF/JPG/SVG
+ Gitlab > code-repo : chacun y poste son travail
+ Les fonctions personnalisées
+ Les [bibliothèques](/generalites/outils.html#les-bibliotheques)
+ Créez un cadavre exquis à partir de différents tableaux (sujets, verbes, compléments). Une phrase doit apparaître à chaque clic de souris, à l'enroit du clic. Le rendu visuel est laissé à votre libre appréciation. + export ds fichier
+ Autre chouette projet pour plus tard: recréer sa tl fb ou twitter ou une tl fictive.
+ Imports d'images, de formes (SVG), de sons, video etc. (> Bibliothèques)
+ Les images : **Charger et utiliser une image** dans un sketch (+ preload).
+ **Sauver** une image
+ Pour nos travaux (en ligne) : [Google sheet](https://docs.google.com/spreadsheets/d/1dxu-R_-tVmMC1_ASeh2irF8KN_TcgBhj4uTuAWUftnw/edit?usp=sharing)
+ **sin()** et **cos()**
+ **lerpColor()**
+ **dist()**, **map()**
+ [HTML, CSS](https://webb2.netlify.com/) etc
+ L'[élément Canvas](https://www.w3schools.com/html/html5_canvas.asp)


# Arrays: Declare, create, assign

https://chevalvert.fr/en/projects/murmur

+ Exemples P5js (Lauren McCarthy): [https://github.com/lmccart/gswp5.js-code](https://github.com/lmccart/gswp5.js-code)

+ [Alan Kay, "The Dynabook—Past Present and Future"](https://www.youtube.com/watch?v=GMDphyKrAE8)
+ https://www.youtube.com/watch?v=r36NNGzNvjo

<a data-fancybox title="P5js" href="/assets/generative.png">![P5js](/assets/generative.png)</a>

[http://www.generative-gestaltung.de/2/](http://www.generative-gestaltung.de/2/)


<a data-fancybox title="Boucles" href="/assets/margot.png">![Boucles](/assets/margot.png)</a>
<small>Le travail de Margot L., à gauche</small>

<a data-fancybox title="Pixels" href="/assets/hanne2.png">![Pixels](/assets/hanne2.png)</a>
<a href="https://fr.wikipedia.org/wiki/Hanne_Darboven" target="_blank"><small><strong>Hanne Darboven</strong>, Construction Drawing, 1968</small></a>