---
title: Manipuler le DOM
lang: fr-FR
---
<a class="backhome" href="/modules-b3/j1.html"><i class="material-icons">keyboard_arrow_left</i>Retour</a>

# Manipuler le DOM

<p class="lead">La page web est composée d'une série d'éléments imbriqués les uns dans les autres ou placés côte à côte. A l'aide de javaScript, et plus spécifiquement jQuery, P5js, Vue.js ou d'autres frameworks ou bibliothèques, nous pouvons manipuler ces éléments, les modifier, les supprimer ou en créer de nouveaux.</p>


<video class="videohtml" controls autoplay>
    <source src="/assets/rafman.mp4" type="video/mp4">
    Your browser does not support the video tag.
</video> 


> [John Rafman, Natura morta](http://natura-morta.com/)

## Exercices

+ Changer la classe d'un élément lorsque l'on clique dessus.
+ Modifier la couleur d'arrière-plan (ou l'image) d'une page web à chaque chargement.

## Editeurs

+ https://developer.mozilla.org/fr/docs/Web/HTML/Element
+ Manipuler le DOM
+ [jQuery](https://api.jquery.com/) / [click](https://api.jquery.com/click/)
Et extensions (Live server, live reload, Sass..)
Rechercher/remplacer
Multifenêtre
Les éditeurs en ligne
Le terminal dans l'interface
NodeJS
L'architecture d'un projet

```javascript
function setup() {
  noCanvas();
}

function draw() {
  createDiv('Everything is getting better every day');
}
```

## Manipulation d'éléments HTML

#### jQuery

+ Qu'est ce que [jQuery](https://fr.wikipedia.org/wiki/JQuery)?
+ [https://jquery.com/](https://jquery.com/) + [Documentation](https://api.jquery.com/)

#### Bootstrap

+ Basic Bootstrap
+ [jQuery](https://jquery.com/) dans Bootstrap
+ Bootstrap 5
+ [Vuepress](https://vuepress.vuejs.org/), [https://vuejs.org/](https://vuejs.org/) React, Angular etc.
+ https://gohugo.io/

#### P5js comme background

L'on peut également utiliser P5js comme background dans une page HTML classique.

#### Fonctions importantes

+ [redraw()](https://p5js.org/reference/#/p5/redraw)