---
title: Dessiner
lang: fr-FR
---

<p class="lead">Le premier workshop d'une série de 6 durant lequel nous introduirons Processing et P5js ainsi que les différents outils et méthodes utilisés lorsque l'on travaille avec du code. Nous aborderons ensuite, à travers le dessin, les grands principes communs à tous les langages de programmation.</p>

# Part 1: Dessin 1

## Au programme

Organisation des workshops

Quelques projets :
Der Spiegel (p5js)
Manipuler le DOM : https://editor.p5js.org/jps723/sketches/Bk_E-k5hZ OU https://editor.p5js.org/jps723/sketches/HJyPPJc3W
Jonathan Puckey (Pointer pointer ou Radio garden, )
Block Bills par Matthias Dorfelt. (aussi ici)
TextFreeBrowsing is a Chrome extension by Jonas Lund & Rafael Rozendaal : http://textfreebrowsing.com/
Interfaces collection : http://lab.gildasp.fr/guic/
US debt clock : http://www.usdebtclock.org/#
Alexei Shulgin : http://archive.rhizome.org/anthology/form-art.html
http://recodeproject.com/ a disparu...
Sonic Pi : https://sonic-pi.net/ ( https://www.youtube.com/watch?v=UrfqA7ShYE0 ou https://www.youtube.com/watch?v=cydH_JAgSfg )
Typography : https://www.schultzschultz.com/G.mp4
Markdown : https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
SASS : https://sass-lang.com/
Machine learning : https://ml5js.org/

Quelques outils 
(Balisage et programmation / Scratch (+dessin), Micro:bit, Node Red, Python, javaScript, SASS + Stylus, etc.. )
Méthodes de travail: Références, organiser ses favoris, tutoriels et documentation
Pour nos travaux : Google sheet
<div class="notification" style="margin-top:4rem;border-left-color:#ff4747;"> <h4>Adresses utiles</h4> <p>Vous trouverez une série d'adresses utiles, de références et de tutoriels sur <a href="adressesutiles.html">cette page</a>. N'hésitez pas à faire des suggestions si vous tombez sur une pépite...</p> </div>

## Processing & P5js

Processing : filiation rapide jusqu'à P5js
Processing ou p5js ? (Puissance de calcul, Web, DOM, Exports PDF, partage, etc...) Nous travaillerons à cheval sur les deux. Dans l'absolu, il est bon de pouvoir passer de l'un à l'autre.
Création de compte sur P5js
L'élément canvas :
Les bibliothèques : https://github.com/bitcraftlab/p5.gui, http://molleindustria.github.io/p5.play/, Son, DOM, BasilJS : http://basiljs.ch/about/
###Exemple

function setup() {
    // Taille de mon élément <canvas>
    createCanvas(windowWidth, windowHeight);
    // Quand une seule valeur est entrée, elle est utilisée pour le rouge, le vert et le bleu (donc toujours niveaux de gris)
    background(240);
}
function draw() {
    // On trace une ligne de la position précédente de la souris jusqu'à sa position actuelle
    line(pmouseX, pmouseY, mouseX, mouseY);
}
function keyPressed() {
    // On exécute le code ci-dessous quand une touche est pressée
    save("mondessin.jpg");
    // On envoie une information dans la console
    console.log("Dessin sauvé! Ouf!");
}

## Why not...

+ createGraphics (Extra canvas) https://www.youtube.com/watch?v=TaluaAD9MKA&index=13&t=0s&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA
+ Drag and drop. Déposer un fichier dans une page et l'utiliser.
+ Websockets
+ Raspberry Pi & Processing for PI
+ Gitbook et générateurs de sites statiques
+ NodeJS
+ Websockets
+ SASS
+ Vuejs
+ Chacun présente une fonction de P5.dom