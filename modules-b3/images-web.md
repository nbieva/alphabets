---
title: Les images
lang: fr-FR
---
<a class="backhome" href="/modules-b3/j1.html"><i class="material-icons">keyboard_arrow_left</i>Retour</a>

# Les images

<p class="lead">Avant de plonger dans la création de notre projet à proprement parler, il nous faut mettre en place les différents éléments qui le compose et un environnement de développement sain et fluide, histoire de ne pas nous préoccuper de soucis techniques inutiles par la suite..</p>

## Images

+ [https://github.com/jpf/wikigifs](https://github.com/jpf/wikigifs)
+ [https://wikiview.net/](https://wikiview.net/)
+ [https://search.creativecommons.org/](https://search.creativecommons.org/)
+ Charger les images dans le canvas et [dessiner avec](https://raygropius.com/the-golden-ratio-trick/).
+ Charger un SVG et dessiner avec
+ Charger des GIFs