---
title: Les objets
lang: fr-FR
---

# Les objets

<iframe class="p5embed" width="740" height="350"  src="https://editor.p5js.org/nicobiev/embed/guu1mm75N"></iframe>

<a data-fancybox title="P5js" href="/assets/generative.png">![P5js](/assets/generative.png)</a>

[http://www.generative-gestaltung.de/2/](http://www.generative-gestaltung.de/2/)

Vous trouverez plus d'informations sur les objets JavaScript sur cette page:

+ https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Utiliser_les_objets
+ https://chevalvert.fr/en/projects/murmur

Dans Processing, une **classe** définit un groupe de méthodes (fonctions) et de propriétés (variables) liées entre elles. Un **objet**, quant à lui, est une instance de cette classe.
Pour utiliser une analogie courante, nous sommes (chacun.e de nous) un objet de la classe Humains et partageons un certain nombre de **fonctions** (manger, respirer, dormir, s'exprimer..) et de **propriétés** (couleur des yeux, taille, tempérament, sexe..) dont les **valeurs** varient de l'un.e à l'autre et nous sont propres.

La **[programmation orientée objet](https://fr.wikipedia.org/wiki/Programmation_orient%C3%A9e_objet)** vous sera très utile si vous manipulez un grand nombre d'éléments, entre autres.
Pour JavaScript, on parlera de programmation orientée **[prototype](https://fr.wikipedia.org/wiki/Programmation_orient%C3%A9e_prototype)**, ce qui est un petit peu différent des objets.

Dans Processing ou P5js, vous pourriez avoir besoin des objets, par exemple, si chacun de vos éléments doit pouvoir évoluer de façon autonome, indépendamment des autres. Par exemple, lorsque vous voulez avoir dans votre page web une série d'ellipses rebondissant sur les bords et changeant de couleur lorsque l'on clique dessus et disparaissant lorsqu'elles se cognent, il faudra capter tous ces événements de façon indépendante (pour chaque éllipse) et non plus de façon globale.

```javascript
function Bubble(x, y) {
  this.x = x;
  this.y = y;
  this.col = color(255, 100);

  this.display = function() {
    stroke(255);
    fill(this.col);
    ellipse(this.x, this.y, 30, 30);
  };

  this.move = function() {
    this.x = this.x + random(-1, 1);
    this.y = this.y + random(-1, 1);

  };
  this.clicked = function() {
    var d = dist(mouseX, mouseY, this.x, this.y);
    if (d < 15) {
      this.col = color(0, random(255), random(100, 255));
    }
  };
}
```

---

## Pour stocker des données

Le format json par exemple, est un format permettant de stocker des données sous forme d'objets.
Voici un événement de votre calendrier
Voici un tweet
Ou un post de Digitalab

## En utilisant les constructeurs:

https://p5js.org/examples/objects-objects.html


## Events

Les événements, en JavaSCript, sont très importants.


Vous trouverez plus d'informations sur les objets JavaScript sur cette page:

+ https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Utiliser_les_objets

```javascript
let boules = []; // On déclare un tableau d'objets

function setup() {
  createCanvas(windowWidth, windowHeight);
  
  noStroke();
  // On crée les objets à l'aide d'une boucle for
  for (let i = 0; i < 1000; i++) {
    boules.push(new Boule()); // On ajoute une boule à chaque boucle
  }
}

function draw() {
  background(0,10);
  for (let i = 0; i < boules.length; i++) {
    boules[i].move();
    boules[i].display();
  }
}

// classe Boule
class Boule {
  constructor() {
    this.x = random(width);
    this.y = random(height);
    this.diameter = random(2, 6);
    this.speed = 5;
  }

  move() {
    this.x += random(-this.speed, this.speed);
    this.y += random(-this.speed, this.speed);
  }

  display() {
    ellipse(this.x, this.y, this.diameter, this.diameter);
  }
}
```