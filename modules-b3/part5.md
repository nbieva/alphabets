---
title: Les données
lang: fr-FR
---

<div style="padding:62% 0 0 0;position:relative;margin:2rem 0;border-radius:5px;"><iframe src="https://player.vimeo.com/video/154839056?color=ffffff" style="position:absolute;top:0;left:0;width:100%;height:100%;border-radius:5px;" frameborder="0" autoplay allow="autoplay; fullscreen" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

> Exhausting a crowd, de [Kyle McDonald](https://kylemcdonald.net/)

# 5. Datas

+ [Moment.js](https://momentjs.com/), une bibliothèque JavaScript pour manipuler le temps et les formats qui y sont liés.
+ Formats json: Une [page très complète](https://la-cascade.io/json-pour-les-debutants/) sur le format.
+ Les objects
+ Data driven documents : https://d3js.org/
+ https://www.zooniverse.org/projects/zooniverse/floating-forests
+ https://www.quora.com/Where-can-I-find-public-or-free-real-time-or-streaming-data-sources
+ https://www.octoparse.com/blog/big-data-70-amazing-free-data-sources-you-should-know-for-2017?qu
+ A simple node package to convert iCal data (.ics file) to JSON format : https://github.com/adrianlee44/ical2json
+ Does the data exists : https://opendatabarometer.org/4thedition/detail-country/?_year=2016&indicator=ODB&detail=BEL

## Datasets & APIs

+ https://github.com/Einenlum/french-verbs-list
+ https://opendata.stib-mivb.be/store/data
+ https://www.infotec.be/fr-be/minformer/opendata.aspx
+ 

<a data-fancybox title="Synchronous Objects" href="/assets/synchronous1.jpg">![William Forsythe's Synchronous Objects for One Flat Thing](/assets/synchronous1.jpg)</a>

<a data-fancybox title="Synchronous Objects" href="/assets/synchronous2.jpg">![William Forsythe's Synchronous Objects for One Flat Thing](/assets/synchronous2.jpg)</a>

[William Forsythe's Synchronous Objects for One Flat Thing](https://synchronousobjects.osu.edu/)

## Pixels array


```processing
PImage img;
float posx, posy, coteH, coteV, nbrH, nbrV;
color maCouleur;

void setup() {
  size(800,800);
  background(0);
  noStroke();
  posx = 0;
  posy = 0;
  nbrH = 800;
  nbrV = 800;
  coteH = width/nbrH;
  coteV  = height/nbrV;
  img = loadImage("earth800.jpg");
  img.loadPixels();
  //translate(0,30);
  for(int i=0; i<img.pixels.length;i++) {
    float r = red(img.pixels[i]);
    float v = green(img.pixels[i]);
    float b = blue(img.pixels[i]);
    float a = alpha(img.pixels[i]);
    /*if(r < 50 && v < 100 && b < 150) {
      
    } else {
      
    }*/
    fill(r,v,b,a);
      rect(posx, posy, coteH, coteV);
    if(posx >= width) {
      posx = 0;
      posy += coteV;
    } else if (posy > height) {
      noLoop();
    }
    posx += coteH;
  }
  img.updatePixels();
}
```